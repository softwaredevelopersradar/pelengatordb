﻿using KvetkaDbPelengator;
using PelengatorModelsDbLib;
using System;
using System.Linq;
using System.Windows;

namespace DbTest
{
    using PelengatorModelsDbLib.ContextModels;
    using System.Net;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        ClientDb clientDb = new ClientDb(IPAddress.Parse("192.168.1.120"));

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            ZadachaEmo a = new ZadachaEmo()
            {
                Datecreate = DateTime.Now,
                IdUsercreate = 1,
                FreqB = "2.0",
                FreqE = "18.0",
                Filter = 1,
                TimeB = DateTime.Now,
                TimeE = DateTime.Now.AddMinutes(2),
                Porog = -110,
                Att = 0,
                IdPost = 2,
                Status = 2,
                Name = "Tester",
                IdZadanie = 0,
                Comment = "It`s my life",
                DeltaIzm = 1,
                IntervalZap = 10,
                IdTaskEx = 1,
                AzimuthMiddle = 0,
                Sector = 360,
                Wavetype = 0
            };

            clientDb?.Tables[NameTable.ZadachaEmo].Add(a);
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            var b = await this.clientDb?.Tables[NameTable.ZadachaEmo].LoadAsync(0);
            var result = b.ToList<ZadachaEmo>();
            ZadachaEmo a = new ZadachaEmo()
            {
                Datecreate = DateTime.Now,
                IdUsercreate = 1,
                FreqB = "1.5",
                FreqE = "30.0",
                Filter = 1,
                TimeB = DateTime.Now,
                TimeE = DateTime.Now.AddMinutes(1),
                Porog = -110,
                Att = 0,
                IdPost = 2,
                Status = 1,
                Name = "Tester",
                IdZadanie = 0,
                Comment = "Serega givi",
                DeltaIzm = 1,
                IntervalZap = 10,
                IdTaskEx = ++result[0].IdTaskEx,
                AzimuthMiddle = 0,
                Sector = 360,
                Wavetype = 0
            };

            await (this.clientDb?.Tables[NameTable.ZadachaEmo].AddAsync(a)).ConfigureAwait(true);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var a = clientDb?.Tables[NameTable.Diagnistics].Load(Convert.ToInt32(Diagnositics.Text));
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            var a = clientDb?.Tables[NameTable.Servers].Load(Convert.ToInt32(Servers.Text));
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            var a = clientDb?.Tables[NameTable.Fhss_IRI].Load(Convert.ToInt32(FHSS_IRI_Text.Text));
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            var a = clientDb?.Tables[NameTable.Fhss_Seans].Load(Convert.ToInt32(FHSS_Seans_Text.Text));
        }

        private async void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var a = await this.clientDb?.Tables[NameTable.Fhss_IRI].LoadAsync(Convert.ToInt32(this.FHSS_IRI_Text.Text));
        }

        private async void ButtonBase_OnClick2(object sender, RoutedEventArgs e)
        {
            var a = await this.clientDb?.Tables[NameTable.Fhss_Seans].LoadAsync(Convert.ToInt32(this.FHSS_Seans_Text.Text));
        }

        private async void Button_Click_5(object sender, RoutedEventArgs e)
        {
            var a = await this.clientDb?.Tables[NameTable.ZadachaEmo].LoadAsync(0);
        }

        private async void Button_Click_6(object sender, RoutedEventArgs e)
        {
            var a = await this.clientDb?.Tables[NameTable.Fhss_IRI_Loacation].LoadAsync(Convert.ToInt32(this.FHSS_Location_Text.Text));
        }

        private async void Button_Click_7(object sender, RoutedEventArgs e)
        {
            var a = await this.clientDb?.Tables[NameTable.ZadachaEmo].LoadAsync(0);
            var result = a.ToList<ZadachaEmo>();
            result[0].Comment = "My way";
            result[0].Status = 2;
            var b = this.clientDb?.Tables[NameTable.ZadachaEmo].Change(result[0]);
        }
    }
}
