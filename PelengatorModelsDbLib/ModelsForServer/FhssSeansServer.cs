﻿using PelengatorModelsDbLib.ContextModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace PelengatorModelsDbLib.ModelsForServer
{
    public class FhssSeansServer : AbstractModel
    {
        public override int Id { get; set; }
        public int IdSeans { get; set; }
        public int IdIri { get; set; }
        public int IdTaskEx { get; set; }
        public double Azimuth { get; set; }
        public double? Sko { get; set; }
        public DateTime? TimeB { get; set; }
        public DateTime? TimeE { get; set; }
        public int? Nfreq { get; set; }
        public int? Klass { get; set; }
        public int? NAllFreq { get; set; }
        public List<double> Freqs { get; set; }
        public List<double> Bounds { get; set; }
        public List<double> AllFreqs { get; set; }
        public List<double> AllBounds { get; set; }
        public List<double> AllAzimuths { get; set; }
        public List<double> AllUgm { get; set; }
        public double Fmin { get; set; }
        public double Fmax { get; set; }
        public double Step { get; set; }

        public override object[] GetKey()
        {
            return new object[] { this.IdTaskEx };
        }

        public override void Update(AbstractModel record)
        {
            var newRecord = (FhssSeans)record;

            this.Id = newRecord.Id;
            this.IdSeans = newRecord.IdSeans;
            this.IdIri = newRecord.IdIri;
            this.IdTaskEx = newRecord.IdTaskEx;
            this.Azimuth = newRecord.Azimuth;
            this.Sko = newRecord.Sko;
            this.TimeB = newRecord.TimeB;
            this.TimeE = newRecord.TimeE;
            this.Nfreq = newRecord.Nfreq;
            this.Klass = newRecord.Klass;
            this.NAllFreq = newRecord.NAllFreq;
            this.Freqs = Parse<double>(newRecord.Freqs);
            this.Bounds = Parse<double>(newRecord.Bounds);
            this.AllFreqs = Parse<double>(newRecord.AllFreqs);
            this.AllBounds = Parse<double>(newRecord.AllBounds);
            this.AllAzimuths = Parse<double>(newRecord.AllAzimuths);
            this.AllUgm = Parse<double>(newRecord.AllUgm);
            this.Fmin = AllFreqs.Min();
            this.Fmax = AllFreqs.Max();
            this.Step = GetStep(Freqs);
        }

        private List<T> Parse<T>(string part) where T : IConvertible
        {
            Type type = typeof(T);

            if (string.IsNullOrEmpty(part))
            {
                return null;
            }

            part = part.Replace(",", ".");
            string[] elements = part.Split(';');
            List<T> result = new List<T>();

            for (int i = 0; i < elements.Length; i++)
            {
                result.Add((T)Convert.ChangeType(elements[i], type, CultureInfo.InvariantCulture));
            }

            return result;
        }

        private double GetStep(List<double> freq)
        {
            List<double> stepList = new List<double>();
            for (int i = 0; i < freq.Count - 1; i++)
            {
                stepList.Add(Math.Abs(freq[i + 1] - freq[i]));
            }
            stepList.RemoveAll(x => x == 0);
            //var d = stepList.GroupBy(x => x).ToDictionary(x => x.Key, x => x.Count());
            //var m = d.FirstOrDefault(x => x.Value == d.Values.Max()).Key;
            return stepList.Min();
        }
    }
}
