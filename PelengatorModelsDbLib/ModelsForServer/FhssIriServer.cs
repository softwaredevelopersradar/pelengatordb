﻿using PelengatorModelsDbLib.ContextModels;
using System;

namespace PelengatorModelsDbLib.ModelsForServer
{
    public class FhssIriServer : AbstractModel
    {
        public override int Id { get; set; }
        public int IdIri { get; set; }
        public int IdTaskEx { get; set; }
        public double? Azimuth { get; set; }
        public double? AzimuthSko { get; set; }
        public DateTime? TimeB { get; set; }
        public DateTime? TimeE { get; set; }
        public int? Nfreq { get; set; }
        public int? Klass { get; set; }
        public int? Nseans { get; set; }
        public double? Freq { get; set; }
        public double? Band { get; set; }
        public double Fmin { get; set; }
        public double Fmax { get; set; }

        public override object[] GetKey()
        {
            return new object[] { this.IdTaskEx };
        }

        public override void Update(AbstractModel record)
        {
            var newRecord = (FhssIri)record;

            this.Id = newRecord.Id;
            this.IdIri = newRecord.IdIri;
            this.IdTaskEx = newRecord.IdTaskEx;
            this.Azimuth = newRecord.Azimuth;
            this.AzimuthSko = newRecord.AzimuthSko;
            this.TimeB = newRecord.TimeB;
            this.TimeE = newRecord.TimeE;
            this.Nfreq = newRecord.Nfreq;
            this.Klass = newRecord.Klass;
            this.Nseans = newRecord.Nseans;
            this.Freq = newRecord.Freq;
            this.Band = newRecord.Band;
            if (Freq != null && Band != null)
            {
                this.Fmin = (double)(newRecord.Freq - (newRecord.Band / 2));
                this.Fmax = (double)(newRecord.Freq + (newRecord.Band / 2));
            }
        }
    }
}
