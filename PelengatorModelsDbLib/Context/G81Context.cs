﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.Context
{
    using System;
    using Microsoft.EntityFrameworkCore;
    using PelengatorModelsDbLib.ContextModels;

    public partial class G81Context : DbContext
    {
        public G81Context()
        {
        }

        public G81Context(DbContextOptions<G81Context> options)
            : base(options)
        {
        }

        #region not used now
        //public virtual DbSet<Afs> Afs { get; set; }
        //public virtual DbSet<AfsComplex> AfsComplex { get; set; }
        //public virtual DbSet<Arm12825e2df1d726b1efPk4> Arm12825e2df1d726b1efPk4 { get; set; }
        //public virtual DbSet<Arms> Arms { get; set; }
        //public virtual DbSet<AutodecScc> AutodecScc { get; set; }
        //public virtual DbSet<Benchmarks> Benchmarks { get; set; }
        //public virtual DbSet<Diagnostics> Diagnostics { get; set; }
        //public virtual DbSet<DiagnosticsArm> DiagnosticsArm { get; set; }
        //public virtual DbSet<DiagnosticsUser> DiagnosticsUser { get; set; }
        //public virtual DbSet<DopplerDeniedMeas> DopplerDeniedMeas { get; set; }
        //public virtual DbSet<DopplerEvents> DopplerEvents { get; set; }
        //public virtual DbSet<DopplerMeas> DopplerMeas { get; set; }
        //public virtual DbSet<Eventhistory> Eventhistory { get; set; }
        //public virtual DbSet<FbPanorama> FbPanorama { get; set; }
        //public virtual DbSet<FhssIriTrigger> FhssIriTrigger { get; set; }
        //public virtual DbSet<FixfreqResult> FixfreqResult { get; set; }
        //public virtual DbSet<HfdlFreqs> HfdlFreqs { get; set; }
        //public virtual DbSet<HfdlGs> HfdlGs { get; set; }
        //public virtual DbSet<HfdlLdfServers> HfdlLdfServers { get; set; }
        //public virtual DbSet<HfdlLpfCut> HfdlLpfCut { get; set; }
        //public virtual DbSet<HfdlLpfResults> HfdlLpfResults { get; set; }
        //public virtual DbSet<HfdlLpfResultsGs> HfdlLpfResultsGs { get; set; }
        //public virtual DbSet<HfdlLpfStats> HfdlLpfStats { get; set; }
        //public virtual DbSet<HfdlResults> HfdlResults { get; set; }
        //public virtual DbSet<HfdlResultsGs> HfdlResultsGs { get; set; }
        //public virtual DbSet<HfdlStats> HfdlStats { get; set; }
        //public virtual DbSet<ImportExport> ImportExport { get; set; }
        //public virtual DbSet<IntegratedFreqLocation> IntegratedFreqLocation { get; set; }
        //public virtual DbSet<IntegratedFreqRecords> IntegratedFreqRecords { get; set; }
        //public virtual DbSet<IntegratedFreqTrigger> IntegratedFreqTrigger { get; set; }
        //public virtual DbSet<IntegratedIriLocation> IntegratedIriLocation { get; set; }
        //public virtual DbSet<IntegratedIriRecords> IntegratedIriRecords { get; set; }
        //public virtual DbSet<IntegratedKlass> IntegratedKlass { get; set; }
        //public virtual DbSet<IntegratedScc> IntegratedScc { get; set; }
        //public virtual DbSet<IntegratedSeansRecords> IntegratedSeansRecords { get; set; }
        //public virtual DbSet<IntegratedSeansTrigger> IntegratedSeansTrigger { get; set; }
        //public virtual DbSet<Interception> Interception { get; set; }
        //public virtual DbSet<InterceptionTask> InterceptionTask { get; set; }
        //public virtual DbSet<IonhaBenchmarks> IonhaBenchmarks { get; set; }
        //public virtual DbSet<IonhaResults> IonhaResults { get; set; }
        //public virtual DbSet<Ionozonds> Ionozonds { get; set; }
        //public virtual DbSet<Iri> Iri { get; set; }
        //public virtual DbSet<IriKlass> IriKlass { get; set; }
        //public virtual DbSet<IriLocation> IriLocation { get; set; }
        //public virtual DbSet<IriMeas> IriMeas { get; set; }
        //public virtual DbSet<ManualPel> ManualPel { get; set; }
        //public virtual DbSet<Netusers> Netusers { get; set; }
        //public virtual DbSet<Network> Network { get; set; }
        //public virtual DbSet<Panorama> Panorama { get; set; }
        //public virtual DbSet<Pelengators> Pelengators { get; set; }
        //public virtual DbSet<Pk5Trigger> Pk5Trigger { get; set; }
        //public virtual DbSet<Pk6Averages> Pk6Averages { get; set; }
        //public virtual DbSet<Pk6Iisa> Pk6Iisa { get; set; }
        //public virtual DbSet<Pk6Redobs> Pk6Redobs { get; set; }
        //public virtual DbSet<Pk6RegResults> Pk6RegResults { get; set; }
        //public virtual DbSet<Pk6Results> Pk6Results { get; set; }
        //public virtual DbSet<Pk6Satellites> Pk6Satellites { get; set; }
        //public virtual DbSet<Pk6SbasAverages> Pk6SbasAverages { get; set; }
        //public virtual DbSet<Pk6SbasResults> Pk6SbasResults { get; set; }
        //public virtual DbSet<PrognozHfc> PrognozHfc { get; set; }
        //public virtual DbSet<RealNetwork> RealNetwork { get; set; }
        //public virtual DbSet<RealNetworkComponents> RealNetworkComponents { get; set; }
        //public virtual DbSet<Repers> Repers { get; set; }
        //public virtual DbSet<Result> Result { get; set; }
        //public virtual DbSet<Seans> Seans { get; set; }
        //public virtual DbSet<SoundTask> SoundTask { get; set; }
        //public virtual DbSet<Sounds> Sounds { get; set; }
        //public virtual DbSet<SpectrParam> SpectrParam { get; set; }
        //public virtual DbSet<Station> Station { get; set; }
        //public virtual DbSet<SystemConnectTable> SystemConnectTable { get; set; }
        //public virtual DbSet<SystemTable> SystemTable { get; set; }
        //public virtual DbSet<TaskEx> TaskEx { get; set; }
        //public virtual DbSet<TaskExDiapazon> TaskExDiapazon { get; set; }
        //public virtual DbSet<Temp12825e2df1d726b1efPk4> Temp12825e2df1d726b1efPk4 { get; set; }
        //public virtual DbSet<TemporaryTables> TemporaryTables { get; set; }
        //public virtual DbSet<Test> Test { get; set; }
        //public virtual DbSet<ToDelete> ToDelete { get; set; }
        //public virtual DbSet<Users> Users { get; set; }
        //public virtual DbSet<VereskInterception> VereskInterception { get; set; }
        //public virtual DbSet<VereskInterceptionTask> VereskInterceptionTask { get; set; }
        //public virtual DbSet<VusrResult> VusrResult { get; set; }
        //public virtual DbSet<ZadachaFix> ZadachaFix { get; set; }
        //public virtual DbSet<ZondHfc> ZondHfc { get; set; }
        //public virtual DbSet<ZondResults> ZondResults { get; set; }
        #endregion

        public virtual DbSet<ZadachaEmo> ZadachaEmo { get; set; }
        public virtual DbSet<Diagnostics> Diagnostics { get; set; }
        public virtual DbSet<Servers> Servers { get; set; }
        public virtual DbSet<FhssIri> FhssIri { get; set; }
        public virtual DbSet<FhssSeans> FhssSeans { get; set; }
        public virtual DbSet<FhssIriLocation> FhssIriLocation { get; set; }

        public DbSet<T> GetTable<T>(NameTable name) where T : class
        {
            switch (name)
            {
                case NameTable.ZadachaEmo:
                    return this.ZadachaEmo as DbSet<T>;
                case NameTable.Diagnistics:
                    return this.Diagnostics as DbSet<T>;
                case NameTable.Servers:
                    return this.Servers as DbSet<T>;
                case NameTable.Fhss_IRI:
                    return this.FhssIri as DbSet<T>;
                case NameTable.Fhss_Seans:
                    return this.FhssSeans as DbSet<T>;
                case NameTable.Fhss_IRI_Loacation:
                    return this.FhssIriLocation as DbSet<T>;
                default:
                    throw new ArgumentOutOfRangeException(nameof(name), name, null);
            }
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=192.168.1.120;Database=G81;User Id=g81;Password=G81;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region NotUsed

            //modelBuilder.Entity<Afs>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("AFS");

            //    entity.Property(e => e.Dt)
            //        .HasColumnName("dt")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.Gr1).HasMaxLength(50);

            //    entity.Property(e => e.Gr2).HasMaxLength(50);

            //    entity.Property(e => e.Gr3).HasMaxLength(50);

            //    entity.Property(e => e.InverPh).HasColumnName("INVER_PH");

            //    entity.Property(e => e.KursAngle).HasColumnName("Kurs_Angle");

            //    entity.Property(e => e.KursAngleRamka).HasColumnName("Kurs_Angle_Ramka");

            //    entity.Property(e => e.Syserror).HasColumnName("SYSERROR");

            //    entity.Property(e => e.TypeAfs).HasColumnName("Type_AFS");

            //    entity.Property(e => e.X).HasMaxLength(200);

            //    entity.Property(e => e.Y).HasMaxLength(200);

            //    entity.Property(e => e.Ze)
            //        .HasColumnName("ZE")
            //        .HasMaxLength(50);
            //});

            //modelBuilder.Entity<AfsComplex>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("AFS_Complex");

            //    entity.Property(e => e.Dt)
            //        .HasColumnName("dt")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.Gr1).HasMaxLength(50);

            //    entity.Property(e => e.Gr2).HasMaxLength(50);

            //    entity.Property(e => e.Gr3).HasMaxLength(50);

            //    entity.Property(e => e.InverPh).HasColumnName("INVER_PH");

            //    entity.Property(e => e.KursAngle).HasColumnName("Kurs_Angle");

            //    entity.Property(e => e.KursAngleRamka).HasColumnName("Kurs_Angle_Ramka");

            //    entity.Property(e => e.Syserror).HasColumnName("SYSERROR");

            //    entity.Property(e => e.TypeAfs).HasColumnName("Type_AFS");

            //    entity.Property(e => e.TypeComplex).HasColumnName("Type_Complex");

            //    entity.Property(e => e.X).HasMaxLength(200);

            //    entity.Property(e => e.Y).HasMaxLength(200);

            //    entity.Property(e => e.Ze)
            //        .HasColumnName("ZE")
            //        .HasMaxLength(50);
            //});

            //modelBuilder.Entity<Arm12825e2df1d726b1efPk4>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("ARM_128_25E_2DF_1D7_26B_1EF_pk4");

            //    entity.Property(e => e.AllDist)
            //        .HasColumnName("All_Dist")
            //        .HasMaxLength(200);

            //    entity.Property(e => e.AlterType).HasColumnName("alter_type");

            //    entity.Property(e => e.ClientFrom)
            //        .HasColumnName("Client_from")
            //        .HasMaxLength(500);

            //    entity.Property(e => e.ClientTo)
            //        .HasColumnName("Client_to")
            //        .HasMaxLength(500);

            //    entity.Property(e => e.DeltaT).HasColumnName("delta_t");

            //    entity.Property(e => e.FilesInfo)
            //        .HasColumnName("Files_info")
            //        .HasMaxLength(100);

            //    entity.Property(e => e.IdIntegratedFreqRecords).HasColumnName("ID_INTEGRATED_FREQ_RECORDS");

            //    entity.Property(e => e.IdServers)
            //        .HasColumnName("ID_Servers")
            //        .HasMaxLength(20);

            //    entity.Property(e => e.Ind)
            //        .HasColumnName("ind")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.Klass).HasMaxLength(1000);

            //    entity.Property(e => e.KlassWeights)
            //        .HasColumnName("Klass_weights")
            //        .HasMaxLength(100);

            //    entity.Property(e => e.LoadPercent).HasColumnName("load_percent");

            //    entity.Property(e => e.Network).HasMaxLength(500);

            //    entity.Property(e => e.NumPoz).HasColumnName("Num_poz");

            //    entity.Property(e => e.NumSeans).HasColumnName("Num_seans");

            //    entity.Property(e => e.Protocol).HasMaxLength(500);

            //    entity.Property(e => e.RaznosPoz).HasColumnName("Raznos_poz");

            //    entity.Property(e => e.Scc)
            //        .HasColumnName("SCC")
            //        .HasMaxLength(1000);

            //    entity.Property(e => e.SkoAzimuth).HasColumnName("SKO_Azimuth");

            //    entity.Property(e => e.SkoUgm).HasColumnName("SKO_Ugm");

            //    entity.Property(e => e.TimeB)
            //        .HasColumnName("time_b")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.TimeE)
            //        .HasColumnName("time_e")
            //        .HasColumnType("datetime");
            //});

            //modelBuilder.Entity<Arms>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("ARMS");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.MacPk)
            //        .HasColumnName("MAC_PK")
            //        .HasMaxLength(100);

            //    entity.Property(e => e.NamePk)
            //        .HasColumnName("Name_PK")
            //        .HasMaxLength(100);

            //    entity.Property(e => e.TypePk).HasColumnName("Type_PK");
            //});

            //modelBuilder.Entity<AutodecScc>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("AUTODEC_SCC");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.Pk2).HasColumnName("PK2");

            //    entity.Property(e => e.Pk3).HasColumnName("PK3");

            //    entity.Property(e => e.Pk41).HasColumnName("PK4_1");

            //    entity.Property(e => e.Pk42).HasColumnName("PK4_2");

            //    entity.Property(e => e.Pk4Def).HasColumnName("PK4_DEF");

            //    entity.Property(e => e.Sccname)
            //        .HasColumnName("SCCNAME")
            //        .HasMaxLength(50)
            //        .IsUnicode(false);
            //});

            //modelBuilder.Entity<Benchmarks>(entity =>
            //{
            //    entity.ToTable("BENCHMARKS");

            //    entity.Property(e => e.Id).HasColumnName("id");

            //    entity.Property(e => e.Active).HasColumnName("active");

            //    entity.Property(e => e.AzimuthPorog)
            //        .HasColumnName("azimuth_porog")
            //        .HasDefaultValueSql("((5))");

            //    entity.Property(e => e.Baseband).HasColumnName("baseband");

            //    entity.Property(e => e.ClassId)
            //        .IsRequired()
            //        .HasColumnName("class_id")
            //        .HasMaxLength(50);

            //    entity.Property(e => e.Description)
            //        .HasColumnName("description")
            //        .HasMaxLength(100);

            //    entity.Property(e => e.Doppler).HasColumnName("doppler");

            //    entity.Property(e => e.DopplerPorog)
            //        .HasColumnName("doppler_porog")
            //        .HasDefaultValueSql("((6))");

            //    entity.Property(e => e.Freq).HasColumnName("freq");

            //    entity.Property(e => e.Klass).HasMaxLength(50);

            //    entity.Property(e => e.Lat).HasColumnName("lat");

            //    entity.Property(e => e.Lon).HasColumnName("lon");

            //    entity.Property(e => e.MeasFreq).HasColumnName("meas_freq");

            //    entity.Property(e => e.MeasPower).HasColumnName("meas_power");

            //    entity.Property(e => e.MeasTime)
            //        .HasColumnName("meas_time")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.MeasUnixtime).HasColumnName("meas_unixtime");

            //    entity.Property(e => e.PowerPorog)
            //        .HasColumnName("power_porog")
            //        .HasDefaultValueSql("((50))");

            //    entity.Property(e => e.PowerPorogNoise)
            //        .IsRequired()
            //        .HasColumnName("power_porog_noise")
            //        .HasDefaultValueSql("('true')");
            //});

            //modelBuilder.Entity<DiagnosticsArm>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("DIAGNOSTICS_ARM");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.NumArm).HasColumnName("num_ARM");

            //    entity.Property(e => e.Status).HasColumnName("status");

            //    entity.Property(e => e.Time)
            //        .HasColumnName("time")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.TypePk).HasColumnName("Type_PK");

            //    entity.Property(e => e.Unixtime).HasColumnName("unixtime");
            //});

            //modelBuilder.Entity<DiagnosticsUser>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("DIAGNOSTICS_USER");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdArms).HasColumnName("ID_ARMS");

            //    entity.Property(e => e.IdUsers).HasColumnName("ID_USERS");

            //    entity.Property(e => e.Status).HasColumnName("status");

            //    entity.Property(e => e.Time)
            //        .HasColumnName("time")
            //        .HasColumnType("datetime");
            //});

            //modelBuilder.Entity<DopplerDeniedMeas>(entity =>
            //{
            //    entity.ToTable("DOPPLER_DENIED_MEAS");

            //    entity.Property(e => e.Id).HasColumnName("id");

            //    entity.Property(e => e.BenchmarkId).HasColumnName("benchmark_id");

            //    entity.Property(e => e.DiagnosticsId).HasColumnName("diagnostics_id");

            //    entity.Property(e => e.NumAzimuth).HasColumnName("num_azimuth");

            //    entity.Property(e => e.NumClass).HasColumnName("num_class");

            //    entity.Property(e => e.NumPower).HasColumnName("num_power");

            //    entity.HasOne(d => d.Benchmark)
            //        .WithMany(p => p.DopplerDeniedMeas)
            //        .HasForeignKey(d => d.BenchmarkId)
            //        .HasConstraintName("FK_DOPPLER_DENIED_MEAS_BENCHMARKS");

            //    entity.HasOne(d => d.Diagnostics)
            //        .WithMany(p => p.DopplerDeniedMeas)
            //        .HasForeignKey(d => d.DiagnosticsId)
            //        .HasConstraintName("FK_DOPPLER_DENIED_MEAS_DIAGNOSTICS");
            //});

            //modelBuilder.Entity<DopplerEvents>(entity =>
            //{
            //    entity.ToTable("DOPPLER_EVENTS");

            //    entity.Property(e => e.Id).HasColumnName("id");

            //    entity.Property(e => e.BenchmarkId).HasColumnName("benchmark_id");

            //    entity.Property(e => e.MeasIdFrom).HasColumnName("meas_id_from");

            //    entity.Property(e => e.MeasIdTo).HasColumnName("meas_id_to");

            //    entity.HasOne(d => d.Benchmark)
            //        .WithMany(p => p.DopplerEvents)
            //        .HasForeignKey(d => d.BenchmarkId)
            //        .OnDelete(DeleteBehavior.ClientSetNull)
            //        .HasConstraintName("FK_DOPPLER_EVENTS_BENCHMARKS");

            //    entity.HasOne(d => d.MeasIdFromNavigation)
            //        .WithMany(p => p.DopplerEvents)
            //        .HasForeignKey(d => d.MeasIdFrom)
            //        .HasConstraintName("FK_DOPPLER_EVENTS_DOPPLER_MEAS");
            //});

            //modelBuilder.Entity<DopplerMeas>(entity =>
            //{
            //    entity.ToTable("DOPPLER_MEAS");

            //    entity.Property(e => e.Id).HasColumnName("id");

            //    entity.Property(e => e.Angle).HasColumnName("angle");

            //    entity.Property(e => e.Azimuth).HasColumnName("azimuth");

            //    entity.Property(e => e.BenchmarkId).HasColumnName("benchmark_id");

            //    entity.Property(e => e.DiagnosticsId).HasColumnName("diagnostics_id");

            //    entity.Property(e => e.Power).HasColumnName("power");

            //    entity.Property(e => e.Shift).HasColumnName("shift");

            //    entity.Property(e => e.Time)
            //        .HasColumnName("time")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.Unixtime).HasColumnName("unixtime");

            //    entity.HasOne(d => d.Benchmark)
            //        .WithMany(p => p.DopplerMeas)
            //        .HasForeignKey(d => d.BenchmarkId)
            //        .HasConstraintName("FK_DOPPLER_MEASUREMENTS_BENCHMARKS");

            //    entity.HasOne(d => d.Diagnostics)
            //        .WithMany(p => p.DopplerMeas)
            //        .HasForeignKey(d => d.DiagnosticsId)
            //        .HasConstraintName("FK_DOPPLER_MEASUREMENTS_DIAGNOSTICS");
            //});

            //modelBuilder.Entity<Eventhistory>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("EVENTHISTORY");

            //    entity.Property(e => e.EventhistoryDocNumber).HasColumnName("eventhistory_doc_number");

            //    entity.Property(e => e.EventhistoryDt)
            //        .HasColumnName("eventhistory_dt")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.EventhistoryEventName)
            //        .HasColumnName("eventhistory_event_name")
            //        .HasMaxLength(100);

            //    entity.Property(e => e.EventhistoryId)
            //        .HasColumnName("eventhistory_id")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.EventhistoryProcessed).HasColumnName("eventhistory_processed");

            //    entity.Property(e => e.EventhistoryTableName)
            //        .HasColumnName("eventhistory_table_name")
            //        .HasMaxLength(100);

            //    entity.Property(e => e.PersonalsId).HasColumnName("personals_id");

            //    entity.Property(e => e.ReceiverId).HasColumnName("receiver_id");

            //    entity.Property(e => e.SenderId).HasColumnName("sender_id");
            //});

            //modelBuilder.Entity<FbPanorama>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("FB_PANORAMA");

            //    entity.Property(e => e.Azimuth).HasColumnType("image");

            //    entity.Property(e => e.AzimuthText)
            //        .HasColumnName("Azimuth_text")
            //        .HasColumnType("text");

            //    entity.Property(e => e.FreqB).HasColumnName("Freq_b");

            //    entity.Property(e => e.FreqStep).HasColumnName("Freq_step");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdTaskEx).HasColumnName("ID_TASK_EX");

            //    entity.Property(e => e.Powers).HasColumnType("image");

            //    entity.Property(e => e.TimeB)
            //        .HasColumnName("time_b")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.TimeBSec).HasColumnName("time_b_sec");

            //    entity.Property(e => e.Ugm).HasColumnType("image");
            //});

            //modelBuilder.Entity<FhssIriTrigger>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("FHSS_IRI_TRIGGER");

            //    entity.HasIndex(e => e.TimeB)
            //        .HasName("IX_Time_b");

            //    entity.HasIndex(e => new { e.IdIri, e.IdTaskEx })
            //        .HasName("IX_ID_IRI_ID_TASK_EX");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdIri).HasColumnName("ID_IRI");

            //    entity.Property(e => e.IdTaskEx).HasColumnName("ID_TASK_EX");

            //    entity.Property(e => e.TimeB)
            //        .HasColumnName("Time_b")
            //        .HasColumnType("datetime");
            //});

            //modelBuilder.Entity<FixfreqResult>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("FIXFREQ_RESULT");

            //    entity.Property(e => e.Demod)
            //        .HasMaxLength(10)
            //        .IsUnicode(false)
            //        .IsFixedLength();

            //    entity.Property(e => e.FilePath)
            //        .HasColumnName("File_path")
            //        .IsUnicode(false);

            //    entity.Property(e => e.FileType).HasColumnName("File_type");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdZadachaFix).HasColumnName("ID_ZADACHA_FIX");

            //    entity.Property(e => e.Sound).HasColumnType("image");

            //    entity.Property(e => e.TimeB)
            //        .HasColumnName("time_b")
            //        .HasColumnType("datetime");
            //});

            //modelBuilder.Entity<HfdlFreqs>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("HFDL_FREQS");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdGs).HasColumnName("ID_GS");
            //});

            //modelBuilder.Entity<HfdlGs>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("HFDL_GS");

            //    entity.Property(e => e.Id).HasColumnName("ID");

            //    entity.Property(e => e.Mesto).HasMaxLength(50);
            //});

            //modelBuilder.Entity<HfdlLdfServers>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("HFDL_LDF_SERVERS");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.LdfId).HasColumnName("LDF_ID");
            //});

            //modelBuilder.Entity<HfdlLpfCut>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("HFDL_LPF_CUT");

            //    entity.Property(e => e.A).HasColumnName("a");

            //    entity.Property(e => e.Alpha).HasColumnName("alpha");

            //    entity.Property(e => e.B).HasColumnName("b");

            //    entity.Property(e => e.CoordFound).HasColumnName("Coord_found");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();
            //});

            //modelBuilder.Entity<HfdlLpfResults>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("HFDL_LPF_RESULTS");

            //    entity.HasIndex(e => new { e.Freq, e.IdHfdlLdfServers, e.TimeRecept, e.Error, e.ErrorDistCut })
            //        .HasName("Ind_HFDL_LPF_RESULTS");

            //    entity.Property(e => e.AzimuthPel).HasColumnName("Azimuth_pel");

            //    entity.Property(e => e.Crc)
            //        .HasColumnName("CRC")
            //        .HasMaxLength(20);

            //    entity.Property(e => e.CrcOk)
            //        .IsRequired()
            //        .HasColumnName("CRC_OK")
            //        .HasMaxLength(5);

            //    entity.Property(e => e.DistCut).HasColumnName("Dist_cut");

            //    entity.Property(e => e.ErrorDistCut).HasColumnName("Error_Dist_cut");

            //    entity.Property(e => e.Flight).HasMaxLength(30);

            //    entity.Property(e => e.Icao).HasColumnName("ICAO");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdHfdlLdfServers).HasColumnName("ID_HFDL_LDF_SERVERS");

            //    entity.Property(e => e.IdHfdlLpfCut).HasColumnName("ID_HFDL_LPF_CUT");

            //    entity.Property(e => e.Register).HasMaxLength(50);

            //    entity.Property(e => e.SkoAz).HasColumnName("SKO_Az");

            //    entity.Property(e => e.TimeMessage)
            //        .HasColumnName("Time_message")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.TimeRecept)
            //        .HasColumnName("Time_recept")
            //        .HasColumnType("datetime");
            //});

            //modelBuilder.Entity<HfdlLpfResultsGs>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("HFDL_LPF_RESULTS_GS");

            //    entity.Property(e => e.AzimuthPel).HasColumnName("Azimuth_pel");

            //    entity.Property(e => e.Crc)
            //        .HasColumnName("CRC")
            //        .HasMaxLength(20);

            //    entity.Property(e => e.CrcOk)
            //        .IsRequired()
            //        .HasColumnName("CRC_OK")
            //        .HasMaxLength(5);

            //    entity.Property(e => e.DistCut).HasColumnName("Dist_cut");

            //    entity.Property(e => e.ErrorDistCut).HasColumnName("Error_Dist_cut");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdGs).HasColumnName("ID_GS");

            //    entity.Property(e => e.IdHfdlLdfServers).HasColumnName("ID_HFDL_LDF_SERVERS");

            //    entity.Property(e => e.IdHfdlLpfCut).HasColumnName("ID_HFDL_LPF_CUT");

            //    entity.Property(e => e.SkoAz).HasColumnName("SKO_Az");

            //    entity.Property(e => e.TimeMessage)
            //        .HasColumnName("Time_message")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.TimeRecept)
            //        .HasColumnName("Time_recept")
            //        .HasColumnType("datetime");
            //});

            //modelBuilder.Entity<HfdlLpfStats>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("HFDL_LPF_STATS");

            //    entity.Property(e => e.AzimuthCntFreq).HasColumnName("Azimuth_cnt_Freq");

            //    entity.Property(e => e.AzimuthCntSeans).HasColumnName("Azimuth_cnt_Seans");

            //    entity.Property(e => e.AzimuthMid).HasColumnName("Azimuth_mid");

            //    entity.Property(e => e.AzimuthSko).HasColumnName("Azimuth_sko");

            //    entity.Property(e => e.DistCutCntFreq).HasColumnName("Dist_cut_cnt_Freq");

            //    entity.Property(e => e.DistCutCntSeans).HasColumnName("Dist_cut_cnt_Seans");

            //    entity.Property(e => e.DistCutSko).HasColumnName("Dist_cut_sko");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdHfdlLdfServers).HasColumnName("ID_HFDL_LDF_SERVERS");

            //    entity.Property(e => e.TimeB)
            //        .HasColumnName("Time_b")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.TimeE)
            //        .HasColumnName("Time_e")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.TypeStat).HasColumnName("type_stat");
            //});

            //modelBuilder.Entity<HfdlResults>(entity =>
            //{
            //    entity.ToTable("HFDL_RESULTS");

            //    entity.HasIndex(e => new { e.Freq, e.Error, e.ErrorTiz, e.ErrorIzmiran, e.ErrorIonha, e.TimeRecept })
            //        .HasName("Ind_HFDL_RESULTS_Time_recept");

            //    entity.Property(e => e.Id).HasColumnName("ID");

            //    entity.Property(e => e.AzimuthPel).HasColumnName("Azimuth_pel");

            //    entity.Property(e => e.Crc)
            //        .HasColumnName("CRC")
            //        .HasMaxLength(20);

            //    entity.Property(e => e.CrcOk)
            //        .IsRequired()
            //        .HasColumnName("CRC_OK")
            //        .HasMaxLength(5);

            //    entity.Property(e => e.DistIonha).HasColumnName("Dist_Ionha");

            //    entity.Property(e => e.DistIzmiran).HasColumnName("Dist_Izmiran");

            //    entity.Property(e => e.DistTiz).HasColumnName("Dist_TIZ");

            //    entity.Property(e => e.DistTiz2).HasColumnName("Dist_TIZ_2");

            //    entity.Property(e => e.ErrorIonha).HasColumnName("Error_Ionha");

            //    entity.Property(e => e.ErrorIzmiran).HasColumnName("Error_Izmiran");

            //    entity.Property(e => e.ErrorTiz).HasColumnName("Error_TIZ");

            //    entity.Property(e => e.ErrorTiz2).HasColumnName("Error_TIZ_2");

            //    entity.Property(e => e.Flight).HasMaxLength(30);

            //    entity.Property(e => e.Icao).HasColumnName("ICAO");

            //    entity.Property(e => e.Register).HasMaxLength(50);

            //    entity.Property(e => e.SkoAz).HasColumnName("SKO_Az");

            //    entity.Property(e => e.SkoUgm).HasColumnName("SKO_Ugm");

            //    entity.Property(e => e.TimeMessage)
            //        .HasColumnName("Time_message")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.TimeRecept)
            //        .HasColumnName("Time_recept")
            //        .HasColumnType("datetime");
            //});

            //modelBuilder.Entity<HfdlResultsGs>(entity =>
            //{
            //    entity.ToTable("HFDL_RESULTS_GS");

            //    entity.Property(e => e.Id).HasColumnName("ID");

            //    entity.Property(e => e.AzimuthPel).HasColumnName("Azimuth_pel");

            //    entity.Property(e => e.Crc)
            //        .HasColumnName("CRC")
            //        .HasMaxLength(20);

            //    entity.Property(e => e.CrcOk)
            //        .HasColumnName("CRC_OK")
            //        .HasMaxLength(5);

            //    entity.Property(e => e.DistIonha).HasColumnName("Dist_Ionha");

            //    entity.Property(e => e.DistIzmiran).HasColumnName("Dist_Izmiran");

            //    entity.Property(e => e.DistTiz).HasColumnName("Dist_TIZ");

            //    entity.Property(e => e.DistTiz2).HasColumnName("Dist_TIZ_2");

            //    entity.Property(e => e.ErrorIonha).HasColumnName("Error_Ionha");

            //    entity.Property(e => e.ErrorIzmiran).HasColumnName("Error_Izmiran");

            //    entity.Property(e => e.ErrorTiz).HasColumnName("Error_TIZ");

            //    entity.Property(e => e.ErrorTiz2).HasColumnName("Error_TIZ_2");

            //    entity.Property(e => e.IdGs).HasColumnName("ID_GS");

            //    entity.Property(e => e.SkoAz).HasColumnName("SKO_Az");

            //    entity.Property(e => e.SkoUgm).HasColumnName("SKO_Ugm");

            //    entity.Property(e => e.TimeMessage)
            //        .HasColumnName("Time_message")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.TimeRecept)
            //        .HasColumnName("Time_recept")
            //        .HasColumnType("datetime");
            //});

            //modelBuilder.Entity<HfdlStats>(entity =>
            //{
            //    entity.ToTable("HFDL_STATS");

            //    entity.HasIndex(e => new { e.TimeB, e.TimeE, e.TypeStat })
            //        .HasName("Ind_HFDL_STATS_Time_status");

            //    entity.Property(e => e.Id).HasColumnName("ID");

            //    entity.Property(e => e.AzimuthCntFreq).HasColumnName("Azimuth_cnt_Freq");

            //    entity.Property(e => e.AzimuthCntSeans).HasColumnName("Azimuth_cnt_Seans");

            //    entity.Property(e => e.AzimuthMid).HasColumnName("Azimuth_mid");

            //    entity.Property(e => e.AzimuthSko).HasColumnName("Azimuth_sko");

            //    entity.Property(e => e.IonhaCntFreq).HasColumnName("Ionha_cnt_Freq");

            //    entity.Property(e => e.IonhaCntSeans).HasColumnName("Ionha_cnt_Seans");

            //    entity.Property(e => e.IonhaSko).HasColumnName("Ionha_sko");

            //    entity.Property(e => e.IzmCntFreq).HasColumnName("Izm_cnt_Freq");

            //    entity.Property(e => e.IzmCntSeans).HasColumnName("Izm_cnt_Seans");

            //    entity.Property(e => e.IzmSko).HasColumnName("Izm_sko");

            //    entity.Property(e => e.TimeB)
            //        .HasColumnName("Time_b")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.TimeE)
            //        .HasColumnName("Time_e")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.TizCntFreq).HasColumnName("TIZ_cnt_Freq");

            //    entity.Property(e => e.TizCntSeans).HasColumnName("TIZ_cnt_Seans");

            //    entity.Property(e => e.TizSko).HasColumnName("TIZ_sko");

            //    entity.Property(e => e.TypeStat).HasColumnName("type_stat");
            //});

            //modelBuilder.Entity<ImportExport>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("IMPORT_EXPORT");

            //    entity.Property(e => e.CurrentTable)
            //        .IsRequired()
            //        .HasColumnName("Current_Table");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdBeforeImport).HasColumnName("ID_before_Import");
            //});

            //modelBuilder.Entity<IntegratedFreqLocation>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("INTEGRATED_FREQ_LOCATION");

            //    entity.HasIndex(e => e.Id)
            //        .HasName("IX_ID")
            //        .IsUnique()
            //        .IsClustered();

            //    entity.HasIndex(e => new { e.Lat, e.Lon, e.AllDist, e.IdIntegratedFreqRecords, e.PositioningTime })
            //        .HasName("IX_ID_INTEGRATED_FREQ_RECORDS");

            //    entity.Property(e => e.A).HasColumnName("a");

            //    entity.Property(e => e.AllDist)
            //        .HasColumnName("All_Dist")
            //        .HasMaxLength(200);

            //    entity.Property(e => e.Alpha).HasColumnName("alpha");

            //    entity.Property(e => e.B).HasColumnName("b");

            //    entity.Property(e => e.CoordFound)
            //        .HasColumnName("Coord_found")
            //        .HasDefaultValueSql("((-1))");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdIntegratedFreqRecords).HasColumnName("ID_INTEGRATED_FREQ_RECORDS");

            //    entity.Property(e => e.NumCalc)
            //        .HasColumnName("Num_calc")
            //        .HasDefaultValueSql("((0))");

            //    entity.Property(e => e.PositioningTime)
            //        .HasColumnName("Positioning_time")
            //        .HasColumnType("datetime");
            //});

            //modelBuilder.Entity<IntegratedFreqRecords>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("INTEGRATED_FREQ_RECORDS");

            //    entity.HasIndex(e => e.Id)
            //        .HasName("IX_ID")
            //        .IsClustered();

            //    entity.HasIndex(e => new { e.CentralFreq, e.Azimuth })
            //        .HasName("IX_FREQ_Azimuth");

            //    entity.HasIndex(e => new { e.Id, e.Azimuth, e.SkoAzimuth, e.AlterType, e.AlterTime, e.CentralFreq })
            //        .HasName("IX_Alter_time");

            //    entity.Property(e => e.AlterTime)
            //        .HasColumnName("alter_time")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.AlterType)
            //        .HasColumnName("alter_type")
            //        .HasDefaultValueSql("((-1))");

            //    entity.Property(e => e.DeltaT).HasColumnName("delta_t");

            //    entity.Property(e => e.FreqIntegrated)
            //        .HasColumnName("freq_integrated")
            //        .HasDefaultValueSql("((-1))");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdsIntegratedKlass)
            //        .HasColumnName("IDS_INTEGRATED_KLASS")
            //        .HasMaxLength(300);

            //    entity.Property(e => e.IdsIntegratedScc)
            //        .HasColumnName("IDS_INTEGRATED_SCC")
            //        .HasMaxLength(300);

            //    entity.Property(e => e.IdsServerFinder)
            //        .HasColumnName("IDS_SERVER_FINDER")
            //        .HasMaxLength(50);

            //    entity.Property(e => e.InterceptStatus).HasColumnName("Intercept_status");

            //    entity.Property(e => e.NumPoz).HasColumnName("Num_poz");

            //    entity.Property(e => e.NumSeans).HasColumnName("Num_seans");

            //    entity.Property(e => e.RaznosPoz).HasColumnName("Raznos_poz");

            //    entity.Property(e => e.SkoAzimuth).HasColumnName("SKO_Azimuth");

            //    entity.Property(e => e.SkoUgm).HasColumnName("SKO_Ugm");

            //    entity.Property(e => e.TimeB)
            //        .HasColumnName("time_b")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.TimeE)
            //        .HasColumnName("time_e")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.WeightKlass)
            //        .HasColumnName("Weight_KLASS")
            //        .HasMaxLength(300);
            //});

            //modelBuilder.Entity<IntegratedFreqTrigger>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("INTEGRATED_FREQ_TRIGGER");

            //    entity.HasIndex(e => e.IdIntegratedFreqRecords)
            //        .HasName("IX_ID_INTEGRATED_FREQ_RECORDS");

            //    entity.HasIndex(e => e.TimeB)
            //        .HasName("IX_Time_b");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdIntegratedFreqRecords).HasColumnName("ID_INTEGRATED_FREQ_RECORDS");

            //    entity.Property(e => e.IdServersFinder).HasColumnName("ID_Servers_finder");

            //    entity.Property(e => e.NumCalc).HasColumnName("Num_calc");

            //    entity.Property(e => e.TimeB)
            //        .HasColumnName("Time_b")
            //        .HasColumnType("datetime");
            //});

            //modelBuilder.Entity<IntegratedIriLocation>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("INTEGRATED_IRI_LOCATION");

            //    entity.HasIndex(e => e.IdIntegratedSeansRecords)
            //        .HasName("IX_ID_INTEGRATED_SEANS_RECORDS");

            //    entity.Property(e => e.A).HasColumnName("a");

            //    entity.Property(e => e.AllDist)
            //        .HasColumnName("All_Dist")
            //        .HasMaxLength(200)
            //        .IsFixedLength();

            //    entity.Property(e => e.Alpha).HasColumnName("alpha");

            //    entity.Property(e => e.B).HasColumnName("b");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdIntegratedSeansRecords).HasColumnName("ID_INTEGRATED_SEANS_RECORDS");

            //    entity.Property(e => e.PositioningTime)
            //        .HasColumnName("Positioning_time")
            //        .HasColumnType("datetime");
            //});

            //modelBuilder.Entity<IntegratedIriRecords>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("INTEGRATED_IRI_RECORDS");

            //    entity.HasIndex(e => e.Id)
            //        .HasName("IX_ID")
            //        .IsUnique()
            //        .IsClustered();

            //    entity.HasIndex(e => e.IdIntegratedSeansRecords)
            //        .HasName("IX_ID_INTEGRATED_SEANS_RECORDS");

            //    entity.HasIndex(e => e.IdServers)
            //        .HasName("IX_ID_Servers");

            //    entity.HasIndex(e => new { e.IdIntegratedFreqRecords, e.TimeB })
            //        .HasName("IX_ID_INTEGRATED_FREQ_RECORDS");

            //    entity.HasIndex(e => new { e.IdServers, e.IdRecord })
            //        .HasName("IX_ID_SERVERS_ID_RECORD");

            //    entity.HasIndex(e => new { e.IdTaskEx, e.IdRecord })
            //        .HasName("IX_ID_TASK_EX_ID_RECORD");

            //    entity.HasIndex(e => new { e.IdIntegratedFreqRecords, e.TimeB, e.TimeE })
            //        .HasName("IX_ID_INTEGRATED_FREQ_RECORDS_TIME_B_E");

            //    entity.Property(e => e.AllDist)
            //        .HasColumnName("All_Dist")
            //        .HasMaxLength(200);

            //    entity.Property(e => e.ClientFrom)
            //        .HasColumnName("Client_from")
            //        .HasMaxLength(50);

            //    entity.Property(e => e.ClientTo)
            //        .HasColumnName("Client_to")
            //        .HasMaxLength(50);

            //    entity.Property(e => e.DeltaT).HasColumnName("delta_t");

            //    entity.Property(e => e.FilesInfo)
            //        .HasColumnName("Files_info")
            //        .HasMaxLength(600);

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdIntegratedFreqRecords).HasColumnName("ID_INTEGRATED_FREQ_RECORDS");

            //    entity.Property(e => e.IdIntegratedSeansRecords).HasColumnName("ID_INTEGRATED_SEANS_RECORDS");

            //    entity.Property(e => e.IdRecord).HasColumnName("ID_RECORD");

            //    entity.Property(e => e.IdServers).HasColumnName("ID_Servers");

            //    entity.Property(e => e.IdTaskEx).HasColumnName("ID_TASK_EX");

            //    entity.Property(e => e.Klass).HasMaxLength(100);

            //    entity.Property(e => e.Network).HasMaxLength(300);

            //    entity.Property(e => e.NumPoz).HasColumnName("Num_poz");

            //    entity.Property(e => e.Protocol).HasMaxLength(100);

            //    entity.Property(e => e.RaznosPoz).HasColumnName("Raznos_poz");

            //    entity.Property(e => e.Scc)
            //        .HasColumnName("SCC")
            //        .HasMaxLength(100);

            //    entity.Property(e => e.SkoAzimuth).HasColumnName("SKO_Azimuth");

            //    entity.Property(e => e.SkoUgm).HasColumnName("SKO_Ugm");

            //    entity.Property(e => e.TimeB)
            //        .HasColumnName("time_b")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.TimeE)
            //        .HasColumnName("time_e")
            //        .HasColumnType("datetime");
            //});

            //modelBuilder.Entity<IntegratedKlass>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("INTEGRATED_KLASS");

            //    entity.HasIndex(e => e.Checksum)
            //        .HasName("IX_CHECKSUM");

            //    entity.HasIndex(e => e.Id)
            //        .HasName("IX_ID");

            //    entity.Property(e => e.Checksum).HasColumnName("CHECKSUM");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.Klass).HasMaxLength(100);
            //});

            //modelBuilder.Entity<IntegratedScc>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("INTEGRATED_SCC");

            //    entity.HasIndex(e => e.Checksum)
            //        .HasName("IX_CHECKSUM");

            //    entity.HasIndex(e => e.Id)
            //        .HasName("IX_ID");

            //    entity.Property(e => e.Checksum).HasColumnName("CHECKSUM");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.Scc)
            //        .HasColumnName("SCC")
            //        .HasMaxLength(100);
            //});

            //modelBuilder.Entity<IntegratedSeansRecords>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("INTEGRATED_SEANS_RECORDS");

            //    entity.HasIndex(e => e.Id)
            //        .HasName("ID")
            //        .IsUnique()
            //        .IsClustered();

            //    entity.HasIndex(e => new { e.TimeB, e.IdIntegratedFreqRecords, e.DeltaT, e.TimeE, e.CentralFreq })
            //        .HasName("IX_time_e");

            //    entity.HasIndex(e => new { e.TimeE, e.IdIntegratedFreqRecords, e.DeltaT, e.TimeB, e.CentralFreq, e.IdServersFinder })
            //        .HasName("IX_time_b");

            //    entity.HasIndex(e => new { e.Azimuth, e.Weight, e.Id, e.TimeB, e.TimeE, e.DeltaT, e.Activ, e.IdIntegratedFreqRecords })
            //        .HasName("IX_ID_FOR_AVERAGE");

            //    entity.Property(e => e.DeltaT).HasColumnName("delta_t");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdIntegratedFreqRecords).HasColumnName("ID_INTEGRATED_FREQ_RECORDS");

            //    entity.Property(e => e.IdServersFinder).HasColumnName("ID_Servers_finder");

            //    entity.Property(e => e.SkoAzimuth).HasColumnName("SKO_Azimuth");

            //    entity.Property(e => e.SkoUgm).HasColumnName("SKO_Ugm");

            //    entity.Property(e => e.TimeB)
            //        .HasColumnName("time_b")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.TimeE)
            //        .HasColumnName("time_e")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.Weight).HasDefaultValueSql("((1))");
            //});

            //modelBuilder.Entity<IntegratedSeansTrigger>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("INTEGRATED_SEANS_TRIGGER");

            //    entity.HasIndex(e => e.IdIntegratedSeansRecords)
            //        .HasName("IX_ID_INTEGRATED_SEANS_RECORDS");

            //    entity.HasIndex(e => e.TimeB)
            //        .HasName("IX_Time_b");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdIntegratedSeansRecords).HasColumnName("ID_INTEGRATED_SEANS_RECORDS");

            //    entity.Property(e => e.IdServersFinder).HasColumnName("ID_Servers_finder");

            //    entity.Property(e => e.NumCalc).HasColumnName("Num_calc");

            //    entity.Property(e => e.TimeB)
            //        .HasColumnName("Time_b")
            //        .HasColumnType("datetime");
            //});

            //modelBuilder.Entity<Interception>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("INTERCEPTION");

            //    entity.HasIndex(e => e.Id)
            //        .HasName("IX_ID")
            //        .IsUnique()
            //        .IsClustered();

            //    entity.HasIndex(e => new { e.IdTaskEx, e.Freq })
            //        .HasName("ind_INTERCEPTION_2");

            //    entity.HasIndex(e => new { e.IdTaskEx, e.IdSignal })
            //        .HasName("ind_INTERCEPTION");

            //    entity.HasIndex(e => new { e.RealTime, e.IdTaskEx, e.Status, e.TimeB, e.ResText, e.ResBin, e.ResSemantic })
            //        .HasName("IX_real_time");

            //    entity.Property(e => e.BinPath)
            //        .HasColumnName("Bin_path")
            //        .HasMaxLength(800);

            //    entity.Property(e => e.BinSize).HasColumnName("bin_size");

            //    entity.Property(e => e.CallsignFrom)
            //        .HasColumnName("callsign_from")
            //        .HasColumnType("text");

            //    entity.Property(e => e.CallsignTo)
            //        .HasColumnName("callsign_to")
            //        .HasColumnType("text");

            //    entity.Property(e => e.DecodPath)
            //        .HasColumnName("Decod_path")
            //        .HasMaxLength(250);

            //    entity.Property(e => e.DecodType).HasColumnName("Decod_type");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdInterceptTask).HasColumnName("ID_INTERCEPT_TASK");

            //    entity.Property(e => e.IdSignal).HasColumnName("ID_SIGNAL");

            //    entity.Property(e => e.IdTaskEx).HasColumnName("ID_TASK_EX");

            //    entity.Property(e => e.IqwTime).HasColumnName("iqw_time");

            //    entity.Property(e => e.Params).HasMaxLength(300);

            //    entity.Property(e => e.RealTime)
            //        .HasColumnName("real_time")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.ResBin).HasColumnName("Res_bin");

            //    entity.Property(e => e.ResSemantic).HasColumnName("Res_semantic");

            //    entity.Property(e => e.ResText).HasColumnName("Res_text");

            //    entity.Property(e => e.Scc)
            //        .HasColumnName("SCC")
            //        .HasMaxLength(250);

            //    entity.Property(e => e.SeansAz).HasColumnName("seans_az");

            //    entity.Property(e => e.SeansB)
            //        .HasColumnName("seans_b")
            //        .HasMaxLength(500)
            //        .IsUnicode(false)
            //        .IsFixedLength();

            //    entity.Property(e => e.SeansD)
            //        .HasColumnName("seans_D")
            //        .HasMaxLength(500)
            //        .IsUnicode(false)
            //        .IsFixedLength();

            //    entity.Property(e => e.SeansE)
            //        .HasColumnName("seans_e")
            //        .HasMaxLength(500)
            //        .IsUnicode(false)
            //        .IsFixedLength();

            //    entity.Property(e => e.SeansKlass)
            //        .HasColumnName("seans_klass")
            //        .HasMaxLength(500)
            //        .IsUnicode(false)
            //        .IsFixedLength();

            //    entity.Property(e => e.SeansScc)
            //        .HasColumnName("seans_scc")
            //        .HasMaxLength(500)
            //        .IsUnicode(false)
            //        .IsFixedLength();

            //    entity.Property(e => e.SeansUgm)
            //        .HasColumnName("seans_ugm")
            //        .HasMaxLength(500)
            //        .IsUnicode(false)
            //        .IsFixedLength();

            //    entity.Property(e => e.SemSize).HasColumnName("sem_size");

            //    entity.Property(e => e.SemanticPath)
            //        .HasColumnName("Semantic_path")
            //        .HasMaxLength(800);

            //    entity.Property(e => e.SignalPath)
            //        .HasColumnName("Signal_path")
            //        .HasMaxLength(250);

            //    entity.Property(e => e.TextPath)
            //        .HasColumnName("Text_path")
            //        .HasMaxLength(800);

            //    entity.Property(e => e.TextSize).HasColumnName("text_size");

            //    entity.Property(e => e.TimeB)
            //        .HasColumnName("Time_b")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.TimeE)
            //        .HasColumnName("Time_e")
            //        .HasColumnType("datetime");

            //    entity.HasOne(d => d.IdInterceptTaskNavigation)
            //        .WithMany()
            //        .HasForeignKey(d => d.IdInterceptTask)
            //        .OnDelete(DeleteBehavior.Cascade)
            //        .HasConstraintName("FK_INTERCEPTION_INTERCEPTION_TASK");
            //});

            //modelBuilder.Entity<InterceptionTask>(entity =>
            //{
            //    entity.ToTable("INTERCEPTION_TASK");

            //    entity.HasIndex(e => new { e.TimeB, e.TimeE, e.RealTime })
            //        .HasName("ind_INTERCEPTION_TASK");

            //    entity.HasIndex(e => new { e.Id, e.Freq, e.Scc, e.IdTaskEx, e.TimeB, e.Status })
            //        .HasName("ind_INTERCEPTION_TASK_2");

            //    entity.Property(e => e.Id).HasColumnName("ID");

            //    entity.Property(e => e.AzimuthMiddle)
            //        .HasColumnName("azimuth_middle")
            //        .HasMaxLength(255);

            //    entity.Property(e => e.BanTime).HasColumnName("ban_time");

            //    entity.Property(e => e.DetectTime).HasColumnName("detect_time");

            //    entity.Property(e => e.IdTaskEx).HasColumnName("ID_TASK_EX");

            //    entity.Property(e => e.InterceptStatus)
            //        .HasColumnName("intercept_status")
            //        .HasDefaultValueSql("((0))");

            //    entity.Property(e => e.Klass).HasMaxLength(500);

            //    entity.Property(e => e.LossRecTime).HasColumnName("loss_rec_time");

            //    entity.Property(e => e.PreRecTime).HasColumnName("pre_rec_time");

            //    entity.Property(e => e.RealTime)
            //        .HasColumnName("real_time")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.RecTime).HasColumnName("rec_time");

            //    entity.Property(e => e.Scc)
            //        .HasColumnName("SCC")
            //        .HasMaxLength(100);

            //    entity.Property(e => e.Sector)
            //        .HasColumnName("sector")
            //        .HasMaxLength(255);

            //    entity.Property(e => e.Status)
            //        .HasColumnName("status")
            //        .HasDefaultValueSql("((1))");

            //    entity.Property(e => e.TimeB)
            //        .HasColumnName("Time_b")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.TimeBan)
            //        .HasColumnName("time_ban")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.TimeE)
            //        .HasColumnName("Time_e")
            //        .HasColumnType("datetime");
            //});

            //modelBuilder.Entity<IonhaBenchmarks>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("IONHA_BENCHMARKS");

            //    entity.Property(e => e.Band).HasColumnName("band");

            //    entity.Property(e => e.Destination)
            //        .HasColumnName("destination")
            //        .HasMaxLength(100);

            //    entity.Property(e => e.Freq).HasColumnName("freq");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("id")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.PowerPorog).HasColumnName("power_porog");
            //});

            //modelBuilder.Entity<IonhaResults>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("IONHA_RESULTS");

            //    entity.HasIndex(e => e.Ind)
            //        .HasName("IND_IONHA_RES");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.Ind).HasColumnName("IND");

            //    entity.Property(e => e.SlopFreq).HasColumnName("Slop_freq");

            //    entity.Property(e => e.Time)
            //        .HasColumnName("time")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.TypeModel).HasColumnName("Type_model");

            //    entity.Property(e => e.VertFreq).HasColumnName("Vert_freq");
            //});

            //modelBuilder.Entity<Ionozonds>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("IONOZONDS");

            //    entity.Property(e => e.FailVi)
            //        .HasColumnName("FailVI")
            //        .HasMaxLength(50);

            //    entity.Property(e => e.Id).HasColumnName("ID");

            //    entity.Property(e => e.Mesto).HasMaxLength(50);

            //    entity.Property(e => e.Name)
            //        .IsRequired()
            //        .HasColumnName("NAME")
            //        .HasMaxLength(50);

            //    entity.Property(e => e.Path).HasMaxLength(100);
            //});

            //modelBuilder.Entity<Iri>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("IRI");

            //    entity.HasIndex(e => new { e.IdIri, e.IdTaskEx })
            //        .HasName("ind_IRI_2");

            //    entity.HasIndex(e => new { e.IdNetwork, e.IdTaskEx })
            //        .HasName("ind_IRI");

            //    entity.HasIndex(e => new { e.IdTaskEx, e.Freq, e.Azimuth })
            //        .HasName("ind_IRI_3");

            //    entity.Property(e => e.AllDist)
            //        .HasColumnName("All_Dist")
            //        .HasMaxLength(200);

            //    entity.Property(e => e.AzimuthB).HasColumnName("Azimuth_b");

            //    entity.Property(e => e.AzimuthE).HasColumnName("Azimuth_e");

            //    entity.Property(e => e.DeltaT).HasColumnName("delta_t");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdIri).HasColumnName("ID_IRI");

            //    entity.Property(e => e.IdIriKlass).HasColumnName("ID_IRI_klass");

            //    entity.Property(e => e.IdNetwork).HasColumnName("ID_network");

            //    entity.Property(e => e.IdRealNetwork)
            //        .HasColumnName("ID_real_network")
            //        .HasDefaultValueSql("((-1))");

            //    entity.Property(e => e.IdTaskEx).HasColumnName("ID_TASK_EX");

            //    entity.Property(e => e.Ind).HasColumnName("IND");

            //    entity.Property(e => e.Sko).HasColumnName("SKO");

            //    entity.Property(e => e.TimeB)
            //        .HasColumnName("time_b")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.TimeE)
            //        .HasColumnName("time_e")
            //        .HasColumnType("datetime");
            //});

            //modelBuilder.Entity<IriKlass>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("IRI_klass");

            //    entity.Property(e => e.A).HasColumnName("a");

            //    entity.Property(e => e.Alpha).HasColumnName("alpha");

            //    entity.Property(e => e.B).HasColumnName("b");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdTaskEx).HasColumnName("id_task_ex");

            //    entity.Property(e => e.NIzm).HasColumnName("n_izm");

            //    entity.Property(e => e.Network).HasColumnName("network");

            //    entity.Property(e => e.Sko).HasColumnName("SKO");

            //    entity.Property(e => e.Status).HasColumnName("status");

            //    entity.Property(e => e.TJob)
            //        .HasColumnName("t_job")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.TimeB)
            //        .HasColumnName("time_b")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.TimeE)
            //        .HasColumnName("time_e")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.Wavetype).HasColumnName("wavetype");
            //});

            //modelBuilder.Entity<IriLocation>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("IRI_LOCATION");

            //    entity.Property(e => e.A).HasColumnName("a");

            //    entity.Property(e => e.Alpha).HasColumnName("alpha");

            //    entity.Property(e => e.B).HasColumnName("b");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdIri).HasColumnName("ID_IRI");

            //    entity.Property(e => e.IdTaskEx).HasColumnName("ID_TASK_EX");
            //});

            //modelBuilder.Entity<IriMeas>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("IRI_MEAS");

            //    entity.HasIndex(e => new { e.IdIri, e.IdTaskEx })
            //        .HasName("ind_IRI_MEAS");

            //    entity.Property(e => e.FreqD).HasColumnName("Freq_d");

            //    entity.Property(e => e.FreqLRaz).HasColumnName("Freq_l_raz");

            //    entity.Property(e => e.FreqLSh).HasColumnName("Freq_l_sh");

            //    entity.Property(e => e.FreqRRaz).HasColumnName("Freq_r_raz");

            //    entity.Property(e => e.FreqRSh).HasColumnName("Freq_r_sh");

            //    entity.Property(e => e.FreqSp).HasColumnName("Freq_sp");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdIri).HasColumnName("ID_IRI");

            //    entity.Property(e => e.IdTaskEx).HasColumnName("ID_TASK_EX");

            //    entity.Property(e => e.Iq)
            //        .HasColumnName("IQ")
            //        .HasColumnType("image");

            //    entity.Property(e => e.IqPath)
            //        .HasColumnName("IQ_path")
            //        .HasMaxLength(250);

            //    entity.Property(e => e.Klass).HasMaxLength(50);

            //    entity.Property(e => e.NumIzm).HasColumnName("Num_izm");

            //    entity.Property(e => e.NumPoz).HasColumnName("Num_poz");

            //    entity.Property(e => e.RaznosPoz).HasColumnName("Raznos_poz");

            //    entity.Property(e => e.Scc)
            //        .HasColumnName("SCC")
            //        .HasMaxLength(100);

            //    entity.Property(e => e.Spectr).HasColumnType("image");

            //    entity.Property(e => e.TimeB)
            //        .HasColumnName("time_b")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.TimeE)
            //        .HasColumnName("time_e")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.USh).HasColumnName("U_sh");
            //});

            //modelBuilder.Entity<ManualPel>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("MANUAL_PEL");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdCom).HasColumnName("ID_com");

            //    entity.Property(e => e.Status).HasColumnName("status");

            //    entity.Property(e => e.TimeB)
            //        .HasColumnName("time_b")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.TimeE)
            //        .HasColumnName("time_e")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.TypePk).HasColumnName("Type_PK");
            //});

            //modelBuilder.Entity<Netusers>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("NETUSERS");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.Login)
            //        .IsRequired()
            //        .HasColumnName("LOGIN")
            //        .HasMaxLength(50);

            //    entity.Property(e => e.MeasRes).HasColumnName("Meas_Res");

            //    entity.Property(e => e.Password)
            //        .IsRequired()
            //        .HasColumnName("PASSWORD")
            //        .HasMaxLength(50);
            //});

            //modelBuilder.Entity<Network>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("NETWORK");

            //    entity.HasIndex(e => new { e.IdNetwork, e.IdTaskEx })
            //        .HasName("ind_network");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdNetwork).HasColumnName("ID_network");

            //    entity.Property(e => e.IdTaskEx).HasColumnName("ID_TASK_EX");
            //});

            //modelBuilder.Entity<Panorama>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("PANORAMA");

            //    entity.HasIndex(e => new { e.IdTaskEx, e.IdTaskExDiap })
            //        .HasName("ind_Panorama");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdTaskEx).HasColumnName("ID_TASK_EX");

            //    entity.Property(e => e.IdTaskExDiap).HasColumnName("ID_TASK_EX_DIAP");

            //    entity.Property(e => e.Spectr).HasColumnType("image");

            //    entity.Property(e => e.TimeB)
            //        .HasColumnName("time_b")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.TimeBMs).HasColumnName("time_b_ms");

            //    entity.Property(e => e.TimeE)
            //        .HasColumnName("time_e")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.TimeEMs).HasColumnName("time_e_ms");
            //});

            //modelBuilder.Entity<Pelengators>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("PELENGATORS");

            //    entity.Property(e => e.FreqB).HasColumnName("Freq_b");

            //    entity.Property(e => e.FreqE).HasColumnName("Freq_e");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.Name)
            //        .IsRequired()
            //        .HasMaxLength(100);
            //});

            //modelBuilder.Entity<Pk5Trigger>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("PK5_TRIGGER");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdRecord).HasColumnName("ID_record");

            //    entity.Property(e => e.NameTable)
            //        .IsRequired()
            //        .HasColumnName("Name_Table")
            //        .HasMaxLength(100);

            //    entity.Property(e => e.SignEmo)
            //        .HasColumnName("Sign_EMO")
            //        .HasDefaultValueSql("('false')");

            //    entity.Property(e => e.TypePk)
            //        .HasColumnName("Type_PK")
            //        .HasDefaultValueSql("((-1))");
            //});

            //modelBuilder.Entity<Pk6Averages>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("PK6_AVERAGES");

            //    entity.HasIndex(e => e.Time)
            //        .HasName("IX_time");

            //    entity.Property(e => e.F0F2).HasColumnName("f0F2");

            //    entity.Property(e => e.Hour).HasColumnName("hour");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.Iisa).HasColumnName("IISA");

            //    entity.Property(e => e.Time)
            //        .HasColumnName("time")
            //        .HasColumnType("datetime");
            //});

            //modelBuilder.Entity<Pk6Iisa>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("PK6_IISA");

            //    entity.HasIndex(e => e.Time)
            //        .HasName("ind_PK6_IISA");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.LatSt).HasColumnName("Lat_st");

            //    entity.Property(e => e.LonSt).HasColumnName("Lon_st");

            //    entity.Property(e => e.NameSt)
            //        .HasColumnName("Name_st")
            //        .HasMaxLength(50);

            //    entity.Property(e => e.Time)
            //        .HasColumnName("time")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.Wtec).HasColumnName("wtec");
            //});

            //modelBuilder.Entity<Pk6Redobs>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("PK6_REDOBS");

            //    entity.HasIndex(e => e.Time)
            //        .HasName("ind_PK6_REDOBS");

            //    entity.Property(e => e.Cmcavrg).HasColumnName("CMCAvrg");

            //    entity.Property(e => e.Cmcstd).HasColumnName("CMCStd");

            //    entity.Property(e => e.DTec).HasColumnName("dTec");

            //    entity.Property(e => e.Freq).HasColumnName("freq");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.Nka).HasColumnName("NKa");

            //    entity.Property(e => e.Prn).HasColumnName("prn");

            //    entity.Property(e => e.S4Correct).HasColumnName("S4_correct");

            //    entity.Property(e => e.Sys).HasColumnName("sys");

            //    entity.Property(e => e.Time)
            //        .HasColumnName("time")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.Type).HasColumnName("type");

            //    entity.Property(e => e._10secSigma).HasColumnName("10SecSigma");

            //    entity.Property(e => e._1secSigma).HasColumnName("1SecSigma");

            //    entity.Property(e => e._30secSigma).HasColumnName("30SecSigma");

            //    entity.Property(e => e._3secSigma).HasColumnName("3SecSigma");

            //    entity.Property(e => e._60secSigma).HasColumnName("60SecSigma");
            //});

            //modelBuilder.Entity<Pk6RegResults>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("PK6_REG_RESULTS");

            //    entity.HasIndex(e => e.Pack)
            //        .HasName("ind_pack");

            //    entity.HasIndex(e => e.Time)
            //        .HasName("IX_time");

            //    entity.Property(e => e.DTec).HasColumnName("dTEC");

            //    entity.Property(e => e.DhD).HasColumnName("dhD");

            //    entity.Property(e => e.DhE).HasColumnName("dhE");

            //    entity.Property(e => e.DhF1).HasColumnName("dhF1");

            //    entity.Property(e => e.DhF2).HasColumnName("dhF2");

            //    entity.Property(e => e.F0D).HasColumnName("f0D");

            //    entity.Property(e => e.F0E).HasColumnName("f0E");

            //    entity.Property(e => e.F0F1).HasColumnName("f0F1");

            //    entity.Property(e => e.F0F2).HasColumnName("f0F2");

            //    entity.Property(e => e.HdF1).HasColumnName("hdF1");

            //    entity.Property(e => e.HdF2).HasColumnName("hdF2");

            //    entity.Property(e => e.HmD).HasColumnName("hmD");

            //    entity.Property(e => e.HmE).HasColumnName("hmE");

            //    entity.Property(e => e.HmF1).HasColumnName("hmF1");

            //    entity.Property(e => e.HmF2).HasColumnName("hmF2");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.Tec).HasColumnName("TEC");

            //    entity.Property(e => e.Time)
            //        .HasColumnName("time")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.WolfI).HasColumnName("Wolf_i");
            //});

            //modelBuilder.Entity<Pk6Results>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("PK6_RESULTS");

            //    entity.HasIndex(e => e.Time)
            //        .HasName("ind_PK6_RESULTS");

            //    entity.Property(e => e.DhD).HasColumnName("dhD");

            //    entity.Property(e => e.DhE).HasColumnName("dhE");

            //    entity.Property(e => e.DhF1).HasColumnName("dhF1");

            //    entity.Property(e => e.DhF2).HasColumnName("dhF2");

            //    entity.Property(e => e.Dtec).HasColumnName("dtec");

            //    entity.Property(e => e.F0D).HasColumnName("f0D");

            //    entity.Property(e => e.F0E).HasColumnName("f0E");

            //    entity.Property(e => e.F0F1).HasColumnName("f0F1");

            //    entity.Property(e => e.F0F2).HasColumnName("f0F2");

            //    entity.Property(e => e.HdF1).HasColumnName("hdF1");

            //    entity.Property(e => e.HdF2).HasColumnName("hdF2");

            //    entity.Property(e => e.HmD).HasColumnName("hmD");

            //    entity.Property(e => e.HmE).HasColumnName("hmE");

            //    entity.Property(e => e.HmF1).HasColumnName("hmF1");

            //    entity.Property(e => e.HmF2).HasColumnName("hmF2");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.KolKa).HasColumnName("kolKa");

            //    entity.Property(e => e.Time)
            //        .HasColumnName("time")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.WolfI).HasColumnName("Wolf_i");
            //});

            //modelBuilder.Entity<Pk6Satellites>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("PK6_SATELLITES");

            //    entity.HasIndex(e => e.Time)
            //        .HasName("IX_Time");

            //    entity.Property(e => e.Azimuth).HasColumnName("azimuth");

            //    entity.Property(e => e.Elevation).HasColumnName("elevation");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.Sat).HasColumnName("sat");

            //    entity.Property(e => e.Sn).HasColumnName("sn");

            //    entity.Property(e => e.Sys).HasColumnName("sys");

            //    entity.Property(e => e.Time)
            //        .HasColumnName("time")
            //        .HasColumnType("datetime");
            //});

            //modelBuilder.Entity<Pk6SbasAverages>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("PK6_SBAS_AVERAGES");

            //    entity.HasIndex(e => e.Time)
            //        .HasName("IX_time");

            //    entity.Property(e => e.F0F2).HasColumnName("f0F2");

            //    entity.Property(e => e.Hour).HasColumnName("hour");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.Iisa).HasColumnName("IISA");

            //    entity.Property(e => e.Time)
            //        .HasColumnName("time")
            //        .HasColumnType("datetime");
            //});

            //modelBuilder.Entity<Pk6SbasResults>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("PK6_SBAS_RESULTS");

            //    entity.HasIndex(e => e.Id)
            //        .HasName("IX_ID")
            //        .IsUnique()
            //        .IsClustered();

            //    entity.HasIndex(e => e.Pack)
            //        .HasName("ind_PK6_SBAS_RESULTS_2");

            //    entity.HasIndex(e => e.Time)
            //        .HasName("ind_PK6_SBAS_RESULTS");

            //    entity.Property(e => e.DTec).HasColumnName("dTEC");

            //    entity.Property(e => e.DhD).HasColumnName("dhD");

            //    entity.Property(e => e.DhE).HasColumnName("dhE");

            //    entity.Property(e => e.DhF1).HasColumnName("dhF1");

            //    entity.Property(e => e.DhF2).HasColumnName("dhF2");

            //    entity.Property(e => e.F0D).HasColumnName("f0D");

            //    entity.Property(e => e.F0E).HasColumnName("f0E");

            //    entity.Property(e => e.F0F1).HasColumnName("f0F1");

            //    entity.Property(e => e.F0F2).HasColumnName("f0F2");

            //    entity.Property(e => e.HdF1).HasColumnName("hdF1");

            //    entity.Property(e => e.HdF2).HasColumnName("hdF2");

            //    entity.Property(e => e.HmD).HasColumnName("hmD");

            //    entity.Property(e => e.HmE).HasColumnName("hmE");

            //    entity.Property(e => e.HmF1).HasColumnName("hmF1");

            //    entity.Property(e => e.HmF2).HasColumnName("hmF2");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.Tec).HasColumnName("TEC");

            //    entity.Property(e => e.Time)
            //        .HasColumnName("time")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.WolfI).HasColumnName("Wolf_i");
            //});

            //modelBuilder.Entity<PrognozHfc>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("PROGNOZ_HFC");

            //    entity.HasIndex(e => new { e.IdZondResults, e.M })
            //        .HasName("ind_PROGNOZ_HFC");

            //    entity.Property(e => e.H).HasColumnName("h");

            //    entity.Property(e => e.IdZondResults).HasColumnName("ID_ZOND_RESULTS");

            //    entity.Property(e => e.Ind)
            //        .HasColumnName("IND")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.M).HasColumnName("m");
            //});

            //modelBuilder.Entity<RealNetwork>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("REAL_NETWORK");

            //    entity.HasIndex(e => new { e.IdRealNetwork, e.IdTaskEx })
            //        .HasName("ind_real_network");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdRealNetwork).HasColumnName("ID_real_network");

            //    entity.Property(e => e.IdTaskEx).HasColumnName("ID_TASK_EX");
            //});

            //modelBuilder.Entity<RealNetworkComponents>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("REAL_NETWORK_COMPONENTS");

            //    entity.HasIndex(e => new { e.IdIri, e.IdTaskEx })
            //        .HasName("ind_Real_net_components");

            //    entity.HasIndex(e => new { e.IdRealNetwork, e.IdTaskEx })
            //        .HasName("ind_Real_net_components_2");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdIri).HasColumnName("ID_IRI");

            //    entity.Property(e => e.IdRealNetwork).HasColumnName("ID_real_network");

            //    entity.Property(e => e.IdTaskEx).HasColumnName("ID_Task_Ex");
            //});

            //modelBuilder.Entity<Repers>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("repers");

            //    entity.Property(e => e.Desc)
            //        .HasColumnName("desc")
            //        .HasMaxLength(150);

            //    entity.Property(e => e.Freq).HasColumnName("freq");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("id")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.Klass)
            //        .HasColumnName("klass")
            //        .HasMaxLength(50);

            //    entity.Property(e => e.Lat).HasColumnName("lat");

            //    entity.Property(e => e.Lon).HasColumnName("lon");

            //    entity.Property(e => e.Mesto)
            //        .HasColumnName("mesto")
            //        .HasMaxLength(500);

            //    entity.Property(e => e.Prim)
            //        .HasColumnName("prim")
            //        .HasMaxLength(3000);

            //    entity.Property(e => e.Scc)
            //        .HasColumnName("scc")
            //        .HasMaxLength(50);

            //    entity.Property(e => e.Url)
            //        .HasColumnName("url")
            //        .HasMaxLength(500);

            //    entity.Property(e => e.Worktime)
            //        .HasColumnName("worktime")
            //        .HasMaxLength(150);
            //});

            //modelBuilder.Entity<Result>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("RESULT");

            //    entity.Property(e => e.Adress)
            //        .HasMaxLength(250)
            //        .IsUnicode(false);

            //    entity.Property(e => e.CallSt)
            //        .HasColumnName("CallST")
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.Callfrom)
            //        .HasColumnName("CALLFROM")
            //        .HasMaxLength(250);

            //    entity.Property(e => e.Callsign)
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.Callto)
            //        .HasColumnName("CALLTO")
            //        .HasMaxLength(250);

            //    entity.Property(e => e.DeltaFr).HasColumnName("delta_Fr");

            //    entity.Property(e => e.DeltaT).HasColumnName("delta_t");

            //    entity.Property(e => e.Depart)
            //        .HasColumnName("depart")
            //        .HasMaxLength(250);

            //    entity.Property(e => e.FreqRpu).HasColumnName("FreqRPU");

            //    entity.Property(e => e.FuncMode)
            //        .HasColumnName("Func_mode")
            //        .HasMaxLength(250);

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdMPel).HasColumnName("ID_M_Pel");

            //    entity.Property(e => e.IdUser).HasColumnName("ID_USER");

            //    entity.Property(e => e.Klass)
            //        .HasColumnName("klass")
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.Mesto)
            //        .HasMaxLength(250)
            //        .IsUnicode(false);

            //    entity.Property(e => e.ModulRpu)
            //        .HasColumnName("ModulRPU")
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.OwnLat).HasColumnName("Own_Lat");

            //    entity.Property(e => e.OwnLon).HasColumnName("Own_Lon");

            //    entity.Property(e => e.PathFile).HasColumnName("Path_file");

            //    entity.Property(e => e.Porog).HasColumnName("porog");

            //    entity.Property(e => e.Post)
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.Prim)
            //        .HasMaxLength(1500)
            //        .IsUnicode(false);

            //    entity.Property(e => e.Protokol)
            //        .HasMaxLength(200)
            //        .IsUnicode(false);

            //    entity.Property(e => e.Radiochange).HasMaxLength(250);

            //    entity.Property(e => e.Radionet).HasMaxLength(250);

            //    entity.Property(e => e.SendProtocol)
            //        .HasColumnName("Send_protocol")
            //        .HasMaxLength(250);

            //    entity.Property(e => e.Skor).HasColumnName("skor");

            //    entity.Property(e => e.SoundPath)
            //        .HasColumnName("Sound_Path")
            //        .HasMaxLength(250)
            //        .IsUnicode(false);

            //    entity.Property(e => e.State)
            //        .HasColumnName("state")
            //        .HasMaxLength(250);

            //    entity.Property(e => e.StationId).HasColumnName("Station_ID");

            //    entity.Property(e => e.Status).HasColumnName("status");

            //    entity.Property(e => e.TaskExFixId).HasColumnName("Task_Ex_FixID");

            //    entity.Property(e => e.TaskExId).HasColumnName("Task_ExID");

            //    entity.Property(e => e.Tk)
            //        .HasColumnName("tk")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.Tn)
            //        .HasColumnName("tn")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.TypePk).HasColumnName("Type_PK");

            //    entity.Property(e => e.TypePr)
            //        .HasColumnName("Type_Pr")
            //        .HasMaxLength(250)
            //        .IsUnicode(false);

            //    entity.Property(e => e.UShir).HasColumnName("u_shir");

            //    entity.Property(e => e.Vladelec)
            //        .HasMaxLength(50)
            //        .IsUnicode(false);
            //});

            //modelBuilder.Entity<Seans>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("SEANS");

            //    entity.HasIndex(e => new { e.IdIri, e.IdTaskEx })
            //        .HasName("ind_Seans");

            //    entity.HasIndex(e => new { e.IdSeans, e.IdTaskEx })
            //        .HasName("ind_Seans_2");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdIri).HasColumnName("ID_IRI");

            //    entity.Property(e => e.IdSeans).HasColumnName("ID_Seans");

            //    entity.Property(e => e.IdTaskEx).HasColumnName("ID_TASK_EX");

            //    entity.Property(e => e.Sko).HasColumnName("SKO");

            //    entity.Property(e => e.Skoa).HasColumnName("skoa");

            //    entity.Property(e => e.TimeB)
            //        .HasColumnName("time_b")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.TimeE)
            //        .HasColumnName("time_e")
            //        .HasColumnType("datetime");
            //});

            //modelBuilder.Entity<SoundTask>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("SOUND_TASK");

            //    entity.HasIndex(e => e.TimeB)
            //        .HasName("ind_sound_task_time");

            //    entity.Property(e => e.Format)
            //        .HasColumnName("format")
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.ModeRpu)
            //        .HasColumnName("mode_RPU")
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.Path).HasMaxLength(255);

            //    entity.Property(e => e.TimeB)
            //        .HasColumnName("time_b")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.TimeE)
            //        .HasColumnName("time_e")
            //        .HasColumnType("datetime");
            //});

            //modelBuilder.Entity<Sounds>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.Property(e => e.Freq).HasColumnName("freq");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.Path)
            //        .HasColumnName("path")
            //        .HasMaxLength(255);

            //    entity.Property(e => e.Tn)
            //        .HasColumnName("tn")
            //        .HasColumnType("datetime");
            //});

            //modelBuilder.Entity<SpectrParam>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("Spectr_param");

            //    entity.Property(e => e.FreqB).HasColumnName("Freq_b");

            //    entity.Property(e => e.FreqStep).HasColumnName("Freq_step");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdRes).HasColumnName("ID_RES");

            //    entity.Property(e => e.Spectr).HasColumnType("text");

            //    entity.Property(e => e.SpectrMax)
            //        .HasColumnName("Spectr_max")
            //        .HasColumnType("text");
            //});

            //modelBuilder.Entity<Station>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.HasIndex(e => new { e.Ss1, e.Vd1 })
            //        .HasName("ind_vd_ss_Station");

            //    entity.Property(e => e.Adres)
            //        .HasColumnName("ADRES")
            //        .HasMaxLength(100)
            //        .IsUnicode(false);

            //    entity.Property(e => e.Antenna)
            //        .HasColumnName("ANTENNA")
            //        .HasMaxLength(100)
            //        .IsUnicode(false);

            //    entity.Property(e => e.Azimut).HasColumnName("AZIMUT");

            //    entity.Property(e => e.Centrede)
            //        .HasColumnName("CENTREDE")
            //        .HasMaxLength(15)
            //        .IsUnicode(false);

            //    entity.Property(e => e.Data)
            //        .HasColumnName("DATA")
            //        .HasColumnType("smalldatetime");

            //    entity.Property(e => e.DataRFr)
            //        .HasColumnName("DATA_R_FR")
            //        .HasColumnType("smalldatetime");

            //    entity.Property(e => e.DataSvR)
            //        .HasColumnName("DATA_SV_R")
            //        .HasColumnType("smalldatetime");

            //    entity.Property(e => e.Fi).HasColumnName("FI");

            //    entity.Property(e => e.Fp).HasColumnName("FP");

            //    entity.Property(e => e.Func)
            //        .HasColumnName("FUNC")
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.Ind)
            //        .HasColumnName("IND")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.Klass)
            //        .HasColumnName("KLASS")
            //        .HasMaxLength(15)
            //        .IsUnicode(false);

            //    entity.Property(e => e.Mesto)
            //        .HasColumnName("MESTO")
            //        .HasMaxLength(150)
            //        .IsUnicode(false);

            //    entity.Property(e => e.Otvetst)
            //        .HasColumnName("OTVETST")
            //        .HasMaxLength(100)
            //        .IsUnicode(false);

            //    entity.Property(e => e.Polariz)
            //        .HasColumnName("POLARIZ")
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.Pozyvnoj)
            //        .HasColumnName("POZYVNOJ")
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.RazrFreq)
            //        .HasColumnName("RAZR_FREQ")
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.Razresch)
            //        .HasColumnName("RAZRESCH")
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.Srok)
            //        .HasColumnName("SROK")
            //        .HasColumnType("smalldatetime");

            //    entity.Property(e => e.SrokRFr)
            //        .HasColumnName("SROK_R_FR")
            //        .HasColumnType("smalldatetime");

            //    entity.Property(e => e.SrokSvR)
            //        .HasColumnName("SROK_SV_R")
            //        .HasColumnType("smalldatetime");

            //    entity.Property(e => e.Ss)
            //        .HasColumnName("SS")
            //        .HasMaxLength(15)
            //        .IsUnicode(false);

            //    entity.Property(e => e.Ss1).HasColumnName("SS1");

            //    entity.Property(e => e.SvReg)
            //        .HasColumnName("SV_REG")
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.TipRes)
            //        .HasColumnName("TIP_RES")
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.Uroven).HasColumnName("UROVEN");

            //    entity.Property(e => e.Usilenie).HasColumnName("USILENIE");

            //    entity.Property(e => e.Vd)
            //        .HasColumnName("VD")
            //        .HasMaxLength(15)
            //        .IsUnicode(false);

            //    entity.Property(e => e.Vd1).HasColumnName("VD1");

            //    entity.Property(e => e.Vid)
            //        .HasColumnName("VID")
            //        .HasMaxLength(10)
            //        .IsUnicode(false);

            //    entity.Property(e => e.Vladelec)
            //        .HasColumnName("VLADELEC")
            //        .HasMaxLength(100)
            //        .IsUnicode(false);

            //    entity.Property(e => e.Zavnomer)
            //        .HasColumnName("ZAVNOMER")
            //        .HasMaxLength(50)
            //        .IsUnicode(false);
            //});

            //modelBuilder.Entity<SystemConnectTable>(entity =>
            //{
            //    entity.ToTable("System_Connect_Table");

            //    entity.HasIndex(e => e.IpBin)
            //        .HasName("Ind_System_Connect_Table_IP_bin");

            //    entity.HasIndex(e => new { e.TimeE, e.StatusLic })
            //        .HasName("Ind_System_Connect_Table_status_Time_e");

            //    entity.Property(e => e.Id).HasColumnName("ID");

            //    entity.Property(e => e.Description).HasMaxLength(200);

            //    entity.Property(e => e.Ip)
            //        .IsRequired()
            //        .HasColumnName("IP")
            //        .HasMaxLength(15);

            //    entity.Property(e => e.IpBin)
            //        .HasColumnName("IP_bin")
            //        .HasMaxLength(4)
            //        .IsFixedLength();

            //    entity.Property(e => e.NamePc)
            //        .HasColumnName("Name_PC")
            //        .HasMaxLength(100);

            //    entity.Property(e => e.StatusLic).HasColumnName("status_lic");

            //    entity.Property(e => e.TimeB)
            //        .HasColumnName("time_b")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.TimeE)
            //        .HasColumnName("time_e")
            //        .HasColumnType("datetime");
            //});

            //modelBuilder.Entity<SystemTable>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("System_Table");

            //    entity.Property(e => e.Description).HasMaxLength(200);

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.TaskName)
            //        .HasColumnName("Task_Name")
            //        .HasMaxLength(100);

            //    entity.Property(e => e.TaskType).HasColumnName("Task_Type");

            //    entity.Property(e => e.Time)
            //        .HasColumnName("time")
            //        .HasColumnType("datetime");
            //});

            //modelBuilder.Entity<TaskEx>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("TASK_EX");

            //    entity.Property(e => e.Att).HasColumnName("att");

            //    entity.Property(e => e.FreqB).HasColumnName("Freq_b");

            //    entity.Property(e => e.FreqE).HasColumnName("Freq_e");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.Operator)
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.Path)
            //        .HasMaxLength(250)
            //        .IsUnicode(false);

            //    entity.Property(e => e.PrtiId).HasColumnName("PRTI_ID");

            //    entity.Property(e => e.Rpu)
            //        .HasColumnName("RPU")
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.Step).HasColumnName("step");

            //    entity.Property(e => e.TaskEmoId).HasColumnName("TASK_EMO_ID");

            //    entity.Property(e => e.TimeB)
            //        .HasColumnName("Time_b")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.TimeE)
            //        .HasColumnName("Time_e")
            //        .HasColumnType("datetime");
            //});

            //modelBuilder.Entity<TaskExDiapazon>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("TASK_EX_DIAPAZON");

            //    entity.HasIndex(e => e.IdTaskEx)
            //        .HasName("ind_Task_ex_diapazon");

            //    entity.Property(e => e.FreqB).HasColumnName("Freq_b");

            //    entity.Property(e => e.FreqE).HasColumnName("Freq_e");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdTaskEx).HasColumnName("ID_TASK_EX");
            //});

            //modelBuilder.Entity<Temp12825e2df1d726b1efPk4>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("Temp_128_25E_2DF_1D7_26B_1EF_pk4");

            //    entity.Property(e => e.AllDist)
            //        .HasColumnName("All_Dist")
            //        .HasMaxLength(200);

            //    entity.Property(e => e.AlterType).HasColumnName("alter_type");

            //    entity.Property(e => e.ClientFrom)
            //        .HasColumnName("Client_from")
            //        .HasMaxLength(500);

            //    entity.Property(e => e.ClientTo)
            //        .HasColumnName("Client_to")
            //        .HasMaxLength(500);

            //    entity.Property(e => e.DeltaT).HasColumnName("delta_t");

            //    entity.Property(e => e.FilesInfo)
            //        .HasColumnName("Files_info")
            //        .HasMaxLength(100);

            //    entity.Property(e => e.IdIntegratedFreqRecords).HasColumnName("ID_INTEGRATED_FREQ_RECORDS");

            //    entity.Property(e => e.IdServers)
            //        .HasColumnName("ID_Servers")
            //        .HasMaxLength(20);

            //    entity.Property(e => e.Ind)
            //        .HasColumnName("ind")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.Klass).HasMaxLength(1000);

            //    entity.Property(e => e.KlassWeights)
            //        .HasColumnName("Klass_weights")
            //        .HasMaxLength(100);

            //    entity.Property(e => e.LoadPercent).HasColumnName("load_percent");

            //    entity.Property(e => e.Network).HasMaxLength(500);

            //    entity.Property(e => e.NumPoz).HasColumnName("Num_poz");

            //    entity.Property(e => e.NumSeans).HasColumnName("Num_seans");

            //    entity.Property(e => e.Protocol).HasMaxLength(500);

            //    entity.Property(e => e.RaznosPoz).HasColumnName("Raznos_poz");

            //    entity.Property(e => e.Scc)
            //        .HasColumnName("SCC")
            //        .HasMaxLength(1000);

            //    entity.Property(e => e.SkoAzimuth).HasColumnName("SKO_Azimuth");

            //    entity.Property(e => e.SkoUgm).HasColumnName("SKO_Ugm");

            //    entity.Property(e => e.TimeB)
            //        .HasColumnName("time_b")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.TimeE)
            //        .HasColumnName("time_e")
            //        .HasColumnType("datetime");
            //});

            //modelBuilder.Entity<TemporaryTables>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("Temporary_Tables");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdServers).HasColumnName("ID_Servers");

            //    entity.Property(e => e.IdTaskEx).HasColumnName("ID_TASK_EX");

            //    entity.Property(e => e.Status).HasColumnName("status");
            //});

            //modelBuilder.Entity<Test>(entity =>
            //{
            //    entity.HasKey(e => e.Ind)
            //        .HasName("PK__TEST__DC50F6C8503BEA1C");

            //    entity.ToTable("TEST");

            //    entity.Property(e => e.Ind).HasColumnName("ind");

            //    entity.Property(e => e.AllDist)
            //        .HasColumnName("All_Dist")
            //        .HasMaxLength(200);

            //    entity.Property(e => e.AlterType).HasColumnName("alter_type");

            //    entity.Property(e => e.ClientFrom)
            //        .HasColumnName("Client_from")
            //        .HasMaxLength(500);

            //    entity.Property(e => e.ClientTo)
            //        .HasColumnName("Client_to")
            //        .HasMaxLength(500);

            //    entity.Property(e => e.DeltaT).HasColumnName("delta_t");

            //    entity.Property(e => e.FilesInfo).HasColumnName("Files_info");

            //    entity.Property(e => e.IdIntegratedFreqRecords).HasColumnName("ID_INTEGRATED_FREQ_RECORDS");

            //    entity.Property(e => e.IdServers)
            //        .HasColumnName("ID_Servers")
            //        .HasMaxLength(20);

            //    entity.Property(e => e.Klass).HasMaxLength(1000);

            //    entity.Property(e => e.KlassWeights)
            //        .HasColumnName("Klass_weights")
            //        .HasMaxLength(100);

            //    entity.Property(e => e.LoadPercent).HasColumnName("load_percent");

            //    entity.Property(e => e.Network).HasMaxLength(500);

            //    entity.Property(e => e.NumPoz).HasColumnName("Num_poz");

            //    entity.Property(e => e.NumSeans).HasColumnName("Num_seans");

            //    entity.Property(e => e.Protocol).HasMaxLength(500);

            //    entity.Property(e => e.RaznosPoz).HasColumnName("Raznos_poz");

            //    entity.Property(e => e.Scc)
            //        .HasColumnName("SCC")
            //        .HasMaxLength(1000);

            //    entity.Property(e => e.SkoAzimuth).HasColumnName("SKO_Azimuth");

            //    entity.Property(e => e.SkoUgm).HasColumnName("SKO_Ugm");

            //    entity.Property(e => e.TimeB)
            //        .HasColumnName("time_b")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.TimeE)
            //        .HasColumnName("time_e")
            //        .HasColumnType("datetime");
            //});

            //modelBuilder.Entity<ToDelete>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.Property(e => e.AllDist)
            //        .HasColumnName("All_Dist")
            //        .HasMaxLength(200);

            //    entity.Property(e => e.AlterType).HasColumnName("alter_type");

            //    entity.Property(e => e.ClientFrom)
            //        .HasColumnName("Client_from")
            //        .HasMaxLength(500);

            //    entity.Property(e => e.ClientTo)
            //        .HasColumnName("Client_to")
            //        .HasMaxLength(500);

            //    entity.Property(e => e.DeltaT).HasColumnName("delta_t");

            //    entity.Property(e => e.FilesInfo)
            //        .HasColumnName("Files_info")
            //        .HasMaxLength(100);

            //    entity.Property(e => e.IdIntegratedFreqRecords).HasColumnName("ID_INTEGRATED_FREQ_RECORDS");

            //    entity.Property(e => e.IdServers)
            //        .HasColumnName("ID_Servers")
            //        .HasMaxLength(20);

            //    entity.Property(e => e.Ind)
            //        .HasColumnName("ind")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.Klass).HasMaxLength(1000);

            //    entity.Property(e => e.KlassWeights)
            //        .HasColumnName("Klass_weights")
            //        .HasMaxLength(100);

            //    entity.Property(e => e.LoadPercent).HasColumnName("load_percent");

            //    entity.Property(e => e.Network).HasMaxLength(500);

            //    entity.Property(e => e.NumPoz).HasColumnName("Num_poz");

            //    entity.Property(e => e.NumSeans).HasColumnName("Num_seans");

            //    entity.Property(e => e.Protocol).HasMaxLength(500);

            //    entity.Property(e => e.RaznosPoz).HasColumnName("Raznos_poz");

            //    entity.Property(e => e.Scc)
            //        .HasColumnName("SCC")
            //        .HasMaxLength(1000);

            //    entity.Property(e => e.SkoAzimuth).HasColumnName("SKO_Azimuth");

            //    entity.Property(e => e.SkoUgm).HasColumnName("SKO_Ugm");

            //    entity.Property(e => e.TimeB)
            //        .HasColumnName("time_b")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.TimeE)
            //        .HasColumnName("time_e")
            //        .HasColumnType("datetime");
            //});

            //modelBuilder.Entity<Users>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("USERS");

            //    entity.Property(e => e.Fname)
            //        .HasColumnName("FName")
            //        .HasMaxLength(50);

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.Lname)
            //        .HasColumnName("LName")
            //        .HasMaxLength(50);

            //    entity.Property(e => e.Login).HasMaxLength(50);

            //    entity.Property(e => e.Mname)
            //        .HasColumnName("MName")
            //        .HasMaxLength(50);

            //    entity.Property(e => e.Password).HasMaxLength(50);

            //    entity.Property(e => e.Post).HasMaxLength(200);
            //});

            //modelBuilder.Entity<VereskInterception>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("VERESK_INTERCEPTION");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdLoid)
            //        .HasColumnName("ID_LOID")
            //        .HasMaxLength(11);

            //    entity.Property(e => e.IdQuery).HasColumnName("ID_QUERY");

            //    entity.Property(e => e.IdTaskEx).HasColumnName("ID_TASK_EX");

            //    entity.Property(e => e.IdVTsignal).HasColumnName("ID_V_TSIGNAL");

            //    entity.Property(e => e.RealTime)
            //        .HasColumnName("real_time")
            //        .HasColumnType("datetime")
            //        .HasDefaultValueSql("(getdate())");

            //    entity.Property(e => e.Type).HasMaxLength(50);
            //});

            //modelBuilder.Entity<VereskInterceptionTask>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("VERESK_INTERCEPTION_TASK");

            //    entity.Property(e => e.Freq).HasColumnName("FREQ");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdQuery).HasColumnName("ID_QUERY");

            //    entity.Property(e => e.IdTaskEx).HasColumnName("ID_TASK_EX");

            //    entity.Property(e => e.RealTime)
            //        .HasColumnName("real_time")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.Status).HasColumnName("status");
            //});

            //modelBuilder.Entity<VusrResult>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("VUSR_RESULT");

            //    entity.Property(e => e.AzArray).HasColumnName("AZ_ARRAY");

            //    entity.Property(e => e.DecPath)
            //        .HasColumnName("dec_path")
            //        .HasMaxLength(250);

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdDetect)
            //        .HasColumnName("ID_detect")
            //        .HasMaxLength(30);

            //    entity.Property(e => e.ProtPath)
            //        .HasColumnName("prot_path")
            //        .HasMaxLength(250);

            //    entity.Property(e => e.TimeB)
            //        .HasColumnName("time_b")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.TimeE)
            //        .HasColumnName("time_e")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.WavPath)
            //        .HasColumnName("wav_path")
            //        .HasMaxLength(250);
            //});
            // modelBuilder.Entity<ZadachaFix>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("ZADACHA_FIX");

            //    entity.Property(e => e.Demod)
            //        .HasMaxLength(10)
            //        .IsUnicode(false)
            //        .IsFixedLength();

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdUsercreate).HasColumnName("ID_usercreate");

            //    entity.Property(e => e.IdZadanie).HasColumnName("ID_zadanie");

            //    entity.Property(e => e.Params)
            //        .HasMaxLength(50)
            //        .IsUnicode(false)
            //        .IsFixedLength();
            //});

            //modelBuilder.Entity<ZondHfc>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("ZOND_HFC");

            //    entity.HasIndex(e => e.IdZondResults)
            //        .HasName("ind_ZOND_HFC");

            //    entity.Property(e => e.H).HasColumnName("h");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdZondResults).HasColumnName("ID_ZOND_RESULTS");
            //});

            //modelBuilder.Entity<ZondResults>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToTable("ZOND_RESULTS");

            //    entity.Property(e => e.EMax).HasColumnName("E_max");

            //    entity.Property(e => e.EMin).HasColumnName("E_min");

            //    entity.Property(e => e.F1Max).HasColumnName("F1_max");

            //    entity.Property(e => e.F1Min).HasColumnName("F1_min");

            //    entity.Property(e => e.F2Max).HasColumnName("F2_max");

            //    entity.Property(e => e.F2Min).HasColumnName("F2_min");

            //    entity.Property(e => e.FEMax).HasColumnName("fE_max");

            //    entity.Property(e => e.FESpMax).HasColumnName("fE_sp_max");

            //    entity.Property(e => e.FF1Max).HasColumnName("fF1_max");

            //    entity.Property(e => e.FF2Max).HasColumnName("fF2_max");

            //    entity.Property(e => e.HE).HasColumnName("hE");

            //    entity.Property(e => e.HEMax).HasColumnName("hE_max");

            //    entity.Property(e => e.HESp).HasColumnName("hE_sp");

            //    entity.Property(e => e.HESpMax).HasColumnName("hE_sp_max");

            //    entity.Property(e => e.HF1).HasColumnName("hF1");

            //    entity.Property(e => e.HF1Max).HasColumnName("hF1_max");

            //    entity.Property(e => e.HF2).HasColumnName("hF2");

            //    entity.Property(e => e.HF2Max).HasColumnName("hF2_max");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("ID")
            //        .ValueGeneratedOnAdd();

            //    entity.Property(e => e.IdIonozonds).HasColumnName("ID_IONOZONDS");

            //    entity.Property(e => e.Picture)
            //        .HasColumnName("PICTURE")
            //        .HasColumnType("image");

            //    entity.Property(e => e.T).HasColumnType("datetime");

            //    entity.Property(e => e.TRealUts)
            //        .HasColumnName("T_real_UTS")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.Wolf).HasColumnName("WOLF");
            //});
            #endregion

            modelBuilder.Entity<ZadachaEmo>(entity =>
            {
                entity.ToTable("ZADACHA_EMO");

                entity.HasIndex(e => new { e.Id, e.IdTaskEx, e.TimeB, e.TimeE, e.IdPost })
                    .HasName("IX_time");

                entity.Property(e => e.Att)
                    .HasColumnName("att")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.AzimuthMiddle)
                    .HasColumnName("azimuth_middle")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Comment)
                    .HasColumnName("COMMENT")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Datecreate)
                    .HasColumnName("DATECREATE")
                    .HasColumnType("datetime");

                entity.Property(e => e.DeltaIzm).HasColumnName("Delta_izm");

                entity.Property(e => e.DocUuid)
                    .HasColumnName("DOC_UUID")
                    .HasMaxLength(250);

                entity.Property(e => e.FreqB)
                    .HasColumnName("Freq_b")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.FreqE)
                    .HasColumnName("Freq_e")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.IdPost).HasColumnName("ID_POST");

                entity.Property(e => e.IdTaskEx).HasColumnName("ID_TASK_EX");

                entity.Property(e => e.IdUsercreate).HasColumnName("ID_USERCREATE");

                entity.Property(e => e.IdZadanie).HasColumnName("ID_ZADANIE");

                entity.Property(e => e.IntervalZap).HasColumnName("interval_zap");

                entity.Property(e => e.Name)
                    .HasColumnName("NAME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Params).HasMaxLength(1000);

                entity.Property(e => e.Porog).HasColumnName("porog");

                entity.Property(e => e.ReportedStatus).HasColumnName("REPORTED_STATUS");

                entity.Property(e => e.Sector)
                    .HasColumnName("sector")
                    .HasDefaultValueSql("((360))");

                entity.Property(e => e.SenderName)
                    .HasColumnName("SENDER_NAME")
                    .HasMaxLength(20);

                entity.Property(e => e.Status).HasColumnName("STATUS");

                entity.Property(e => e.TaskId).HasColumnName("TASK_ID");

                entity.Property(e => e.TaskUuid)
                    .HasColumnName("TASK_UUID")
                    .HasMaxLength(250);

                entity.Property(e => e.TimeB)
                    .HasColumnName("time_b")
                    .HasColumnType("datetime");

                entity.Property(e => e.TimeE)
                    .HasColumnName("time_e")
                    .HasColumnType("datetime");

                entity.Property(e => e.Wavetype)
                    .HasColumnName("wavetype")
                    .HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<Diagnostics>(entity =>
            {
                entity.ToTable("DIAGNOSTICS");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.IdServers).HasColumnName("ID_Servers");

                entity.Property(e => e.Status).HasColumnName("status");

                entity.Property(e => e.Time)
                    .HasColumnName("time")
                    .HasColumnType("datetime");

                entity.Property(e => e.TypeDiagObj).HasColumnName("Type_DiagObj");

                entity.Property(e => e.Unixtime).HasColumnName("unixtime");
            });

            modelBuilder.Entity<Servers>(entity =>
            {
                entity.ToTable("SERVERS");

                entity.Property(e => e.AddDescript).HasColumnName("Add_descript");

                entity.Property(e => e.Default).HasDefaultValueSql("((0))");

                entity.Property(e => e.FreqB)
                    .HasColumnName("Freq_b")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.FreqE)
                    .HasColumnName("Freq_e")
                    .HasDefaultValueSql("((30))");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Ip)
                    .HasColumnName("IP")
                    .HasMaxLength(50);

                entity.Property(e => e.Login).HasMaxLength(50);

                entity.Property(e => e.Mesto).HasMaxLength(50);

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.Password).HasMaxLength(50);

                entity.Property(e => e.PortBin)
                    .HasColumnName("Port_bin")
                    .HasMaxLength(50);

                entity.Property(e => e.PortSnd)
                    .HasColumnName("Port_snd")
                    .HasMaxLength(50);

                entity.Property(e => e.PortUnp)
                    .HasColumnName("Port_unp")
                    .HasMaxLength(50);

                entity.Property(e => e.TimeOut).HasMaxLength(50);

                entity.Property(e => e.TypePk).HasColumnName("Type_PK");

                entity.Property(e => e.TypeServer).HasColumnName("Type_Server");
            });

            modelBuilder.Entity<FhssIri>(entity =>
            {
                entity.ToTable("FHSS_IRI");

                entity.HasIndex(e => new { e.IdIri, e.IdTaskEx })
                    .HasName("ind_fhss_iri");

                entity.HasIndex(e => new { e.IdIri, e.IdTaskEx, e.Azimuth, e.Klass, e.IdSeanses, e.TimeB, e.TimeE, e.Freq })
                    .HasName("ind_fhss_iri_for_pel");

                entity.Property(e => e.AzimuthSko).HasColumnName("Azimuth_sko");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedOnAdd().HasDefaultValueSql("((0))"); ;

                entity.Property(e => e.IdIri).HasColumnName("ID_IRI");

                entity.Property(e => e.IdSeanses)
                    .HasColumnName("ID_Seanses")
                    .HasColumnType("nvarchar(max)");

                entity.Property(e => e.IdTaskEx).HasColumnName("ID_TASK_EX");

                entity.Property(e => e.TimeB)
                    .HasColumnName("time_b")
                    .HasColumnType("datetime");

                entity.Property(e => e.TimeE)
                    .HasColumnName("time_e")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<FhssSeans>(entity =>
            {
                entity.ToTable("FHSS_SEANS");

                entity.HasIndex(e => e.Id)
                    .HasName("ind_fhss_seans_id")
                    .IsUnique();

                entity.HasIndex(e => new { e.IdSeans, e.IdTaskEx })
                    .HasName("ind_fhss_seans");

                entity.HasIndex(e => new { e.IdTaskEx, e.IdIri })
                    .HasName("ind_fhss_seans_for_pel");

                entity.Property(e => e.AllAzimuths).HasColumnName("All_azimuths");

                entity.Property(e => e.AllBounds).HasColumnName("All_bounds");

                entity.Property(e => e.AllFreqs).HasColumnName("All_freqs");

                entity.Property(e => e.AllTimeB).HasColumnName("All_time_b");

                entity.Property(e => e.AllTimeE).HasColumnName("All_time_e");

                entity.Property(e => e.AllUgm).HasColumnName("All_ugm");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedOnAdd().HasDefaultValueSql("((0))");

                entity.Property(e => e.IdIri).HasColumnName("ID_IRI");

                entity.Property(e => e.IdSeans).HasColumnName("ID_SEANS");

                entity.Property(e => e.IdTaskEx).HasColumnName("ID_TASK_EX");

                entity.Property(e => e.NAllFreq).HasColumnName("N_all_freq");

                entity.Property(e => e.PosBandAv).HasColumnName("Pos_band_av");

                entity.Property(e => e.PosBandMax).HasColumnName("Pos_band_max");

                entity.Property(e => e.PosBandSko).HasColumnName("Pos_band_sko");

                entity.Property(e => e.PosBySec).HasColumnName("Pos_by_sec");

                entity.Property(e => e.TPos).HasColumnName("T_pos");

                entity.Property(e => e.TPosSko).HasColumnName("T_pos_sko");

                entity.Property(e => e.TimeB)
                    .HasColumnName("time_b")
                    .HasColumnType("datetime");

                entity.Property(e => e.TimeE)
                    .HasColumnName("time_e")
                    .HasColumnType("datetime");

                entity.Property(e => e.Waveform).HasMaxLength(250);
            });

            modelBuilder.Entity<FhssIriLocation>(entity =>
            {
                entity.ToTable("FHSS_IRI_LOCATION");

                entity.HasIndex(e => new { e.IdIri, e.IdTaskEx })
                    .HasName("IX_ID_IRI_ID_TASK_EX");

                entity.Property(e => e.A).HasColumnName("a");

                entity.Property(e => e.AllDist)
                    .HasColumnName("All_Dist")
                    .HasMaxLength(200);

                entity.Property(e => e.Alpha).HasColumnName("alpha");

                entity.Property(e => e.B).HasColumnName("b");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedOnAdd().HasDefaultValueSql("((0))");

                entity.Property(e => e.IdIri).HasColumnName("ID_IRI");

                entity.Property(e => e.IdTaskEx).HasColumnName("ID_TASK_EX");

                entity.Property(e => e.PositioningTime)
                    .HasColumnName("Positioning_time")
                    .HasColumnType("datetime");
            });

            this.OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
