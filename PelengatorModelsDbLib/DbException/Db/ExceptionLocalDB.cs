﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PelengatorModelsDbLib
{
    /// <summary>
    /// Класс для передачи ошибок от Бд внутри сервиса
    /// </summary>
    [Serializable]
    public class ExceptionLocalDB : ApplicationException
    {
        public EnumDBError Error { get; protected set; }

        public ExceptionLocalDB(EnumDBError error) : base(DBError.GetDefenition(error))
        {
            Error = error;
        }

        public ExceptionLocalDB(string message) : base(message)
        {
            Error = EnumDBError.UnknownError;
        }

    }
}
