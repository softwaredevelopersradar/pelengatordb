﻿namespace PelengatorModelsDbLib
{
    public interface IExceptionDb
    {
        EnumDBError Error { get; }

        string Message { get; }
    }
}
