﻿using System;
using System.Collections.Generic;

namespace PelengatorModelsDbLib
{
    public class ClientError
    {
        public static string GetDefenition(EnumClientError error)
        {
            try { return DicError[error]; }
            catch (Exception) { return ""; }
        }

        private static Dictionary<EnumClientError, string> DicError = new Dictionary<EnumClientError, string>
        {
            { EnumClientError.UnknownError, "Error: "},
            { EnumClientError.NoConnection, "Error: No Connection! "},
            { EnumClientError.IncorrectEndpoint, "Error: Endpoint is incorrect!"}
        };
    }
}
