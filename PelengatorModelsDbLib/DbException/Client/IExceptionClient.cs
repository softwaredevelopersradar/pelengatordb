﻿namespace PelengatorModelsDbLib
{
    public interface IExceptionClient
    {
        EnumClientError Error { get; }

        string Message { get; }
    }
}
