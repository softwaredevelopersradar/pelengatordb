﻿using System.Runtime.Serialization;

namespace PelengatorModelsDbLib
{
    [DataContract]
    public enum NameTable : byte
    {
        [EnumMember]
        ZadachaEmo,
        [EnumMember]
        Diagnistics,
        [EnumMember]
        Servers,
        [EnumMember]
        Fhss_IRI,
        [EnumMember]
        Fhss_Seans,
        [EnumMember]
        Fhss_IRI_Loacation
    }

    [DataContract]
    public enum EnumDBError : byte
    {
        [EnumMember]
        UnknownError,
        [EnumMember]
        RecordExist,
        [EnumMember]
        RecordNotFound,
        [EnumMember]
        None
    }

    [DataContract]
    public enum EnumClientError : byte
    {
        [EnumMember]
        None,
        [EnumMember]
        UnknownError,
        [EnumMember]
        IncorrectEndpoint,
        [EnumMember]
        NoConnection
    }
}
