﻿using System;

namespace PelengatorModelsDbLib
{
    public class InfoTableAttribute : Attribute
    {
        public NameTable Name { get; private set; }

        public InfoTableAttribute(NameTable name)
        {
            Name = name;
        }
    }
}
