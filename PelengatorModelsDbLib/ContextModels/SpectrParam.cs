﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    public partial class SpectrParam
    {
        public int Id { get; set; }
        public int? IdRes { get; set; }
        public double? FreqB { get; set; }
        public double? FreqStep { get; set; }
        public string Spectr { get; set; }
        public string SpectrMax { get; set; }
    }
}
