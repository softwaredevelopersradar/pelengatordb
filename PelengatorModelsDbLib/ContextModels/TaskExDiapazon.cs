﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    public partial class TaskExDiapazon
    {
        public int Id { get; set; }
        public int? IdTaskEx { get; set; }
        public double? FreqB { get; set; }
        public double? FreqE { get; set; }
        public double? Filter { get; set; }
    }
}
