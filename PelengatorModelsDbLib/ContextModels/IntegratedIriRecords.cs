﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class IntegratedIriRecords
    {
        public int Id { get; set; }
        public int IdRecord { get; set; }
        public int? IdTaskEx { get; set; }
        public int? IdServers { get; set; }
        public int? IdIntegratedFreqRecords { get; set; }
        public int? IdIntegratedSeansRecords { get; set; }
        public double? Freq { get; set; }
        public double? CentralFreq { get; set; }
        public double? Azimuth { get; set; }
        public double? SkoAzimuth { get; set; }
        public double? Ugm { get; set; }
        public double? SkoUgm { get; set; }
        public double? Polosa { get; set; }
        public double? U { get; set; }
        public int? Skor { get; set; }
        public int? RaznosPoz { get; set; }
        public int? NumPoz { get; set; }
        public string Klass { get; set; }
        public string Scc { get; set; }
        public DateTime? TimeB { get; set; }
        public DateTime? TimeE { get; set; }
        public double? DeltaT { get; set; }
        public int? Wavetype { get; set; }
        public double? Dist { get; set; }
        public string AllDist { get; set; }
        public string Network { get; set; }
        public string Protocol { get; set; }
        public string ClientFrom { get; set; }
        public string ClientTo { get; set; }
        public int? Classified { get; set; }
        public string FilesInfo { get; set; }
    }
}
