﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class Pk6SbasResults
    {
        public int Id { get; set; }
        public DateTime? Time { get; set; }
        public int Pack { get; set; }
        public double Lat { get; set; }
        public double Lon { get; set; }
        public int WolfI { get; set; }
        public double? F0F2 { get; set; }
        public double? HmF2 { get; set; }
        public double? DhF2 { get; set; }
        public double? F0F1 { get; set; }
        public double? HmF1 { get; set; }
        public double? DhF1 { get; set; }
        public double? F0E { get; set; }
        public double? HmE { get; set; }
        public double? DhE { get; set; }
        public double? Tec { get; set; }
        public double? DTec { get; set; }
        public double? F0D { get; set; }
        public double? HmD { get; set; }
        public double? DhD { get; set; }
        public double? HdF2 { get; set; }
        public double? HdF1 { get; set; }
    }
}
