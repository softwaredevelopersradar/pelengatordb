﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class Pk6Redobs
    {
        public int Id { get; set; }
        public DateTime Time { get; set; }
        public int Prn { get; set; }
        public int? Freq { get; set; }
        public int? Sys { get; set; }
        public int? Type { get; set; }
        public double? Azimuth { get; set; }
        public double? Elevation { get; set; }
        public double? Nka { get; set; }
        public double? LockTime { get; set; }
        public double? Cmcavrg { get; set; }
        public double? Cmcstd { get; set; }
        public double? S4 { get; set; }
        public double? S4Correct { get; set; }
        public double? _1secSigma { get; set; }
        public double? _3secSigma { get; set; }
        public double? _10secSigma { get; set; }
        public double? _30secSigma { get; set; }
        public double? _60secSigma { get; set; }
        public double? Tec { get; set; }
        public double? DTec { get; set; }
    }
}
