﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class IntegratedSeansRecords
    {
        public int Id { get; set; }
        public int? IdIntegratedFreqRecords { get; set; }
        public double? CentralFreq { get; set; }
        public DateTime? TimeB { get; set; }
        public DateTime? TimeE { get; set; }
        public double? DeltaT { get; set; }
        public double? Azimuth { get; set; }
        public double? SkoAzimuth { get; set; }
        public double? Ugm { get; set; }
        public double? SkoUgm { get; set; }
        public double? U { get; set; }
        public int Weight { get; set; }
        public int? IdServersFinder { get; set; }
        public int? Activ { get; set; }
    }
}
