﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class HfdlLpfStats
    {
        public int Id { get; set; }
        public int IdHfdlLdfServers { get; set; }
        public double? Freq { get; set; }
        public DateTime? TimeB { get; set; }
        public DateTime? TimeE { get; set; }
        public int? TypeStat { get; set; }
        public double? AzimuthMid { get; set; }
        public double? AzimuthSko { get; set; }
        public int? AzimuthCntFreq { get; set; }
        public int? AzimuthCntSeans { get; set; }
        public double? DistCutSko { get; set; }
        public int? DistCutCntFreq { get; set; }
        public int? DistCutCntSeans { get; set; }
    }
}
