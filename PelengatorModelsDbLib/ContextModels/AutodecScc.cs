﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    public partial class AutodecScc
    {
        public int Id { get; set; }
        public string Sccname { get; set; }
        public int Pk2 { get; set; }
        public int Pk3 { get; set; }
        public int Pk41 { get; set; }
        public int Pk42 { get; set; }
        public int Pk4Def { get; set; }
    }
}
