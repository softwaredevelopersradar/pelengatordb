﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    public partial class Network
    {
        public int Id { get; set; }
        public int? IdNetwork { get; set; }
        public double? Freq { get; set; }
        public double? Polosa { get; set; }
        public int IdTaskEx { get; set; }
    }
}
