﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class ZondResults
    {
        public int Id { get; set; }
        public DateTime? T { get; set; }
        public int? Wolf { get; set; }
        public double? Lat { get; set; }
        public double? Lon { get; set; }
        public double? EMin { get; set; }
        public double? EMax { get; set; }
        public double? F1Min { get; set; }
        public double? F1Max { get; set; }
        public double? F2Min { get; set; }
        public double? F2Max { get; set; }
        public double? HEMax { get; set; }
        public double? FEMax { get; set; }
        public double? HESpMax { get; set; }
        public double? FESpMax { get; set; }
        public double? HE { get; set; }
        public double? HESp { get; set; }
        public double? HF1Max { get; set; }
        public double? FF1Max { get; set; }
        public double? HF1 { get; set; }
        public double? HF2Max { get; set; }
        public double? FF2Max { get; set; }
        public double? HF2 { get; set; }
        public DateTime? TRealUts { get; set; }
        public int? IdIonozonds { get; set; }
        public byte[] Picture { get; set; }
    }
}
