﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    public partial class ZadachaFix
    {
        public int Id { get; set; }
        public double? Freq { get; set; }
        public string Demod { get; set; }
        public int? Prioritet { get; set; }
        public int? IdUsercreate { get; set; }
        public int? IdZadanie { get; set; }
        public string Params { get; set; }
    }
}
