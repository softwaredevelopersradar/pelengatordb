﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    public partial class DopplerDeniedMeas
    {
        public int Id { get; set; }
        public int DiagnosticsId { get; set; }
        public int BenchmarkId { get; set; }
        public int NumPower { get; set; }
        public int NumAzimuth { get; set; }
        public int NumClass { get; set; }

        public virtual Benchmarks Benchmark { get; set; }
        public virtual Diagnostics Diagnostics { get; set; }
    }
}
