﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    public partial class RealNetworkComponents
    {
        public int Id { get; set; }
        public int IdIri { get; set; }
        public int IdRealNetwork { get; set; }
        public int IdTaskEx { get; set; }
    }
}
