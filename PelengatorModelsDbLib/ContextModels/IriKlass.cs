﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class IriKlass
    {
        public int Id { get; set; }
        public double? Freq { get; set; }
        public double? Shirina { get; set; }
        public double? Azimuth { get; set; }
        public double? Sko { get; set; }
        public double? Lat { get; set; }
        public double? Lon { get; set; }
        public double? A { get; set; }
        public double? B { get; set; }
        public double? Alpha { get; set; }
        public DateTime? TimeB { get; set; }
        public DateTime? TimeE { get; set; }
        public DateTime? TJob { get; set; }
        public double? U { get; set; }
        public int? NIzm { get; set; }
        public int? Wavetype { get; set; }
        public int? Network { get; set; }
        public int? IdTaskEx { get; set; }
        public int? Status { get; set; }
    }
}
