﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    public partial class ImportExport
    {
        public int Id { get; set; }
        public int Status { get; set; }
        public string CurrentTable { get; set; }
        public int? IdBeforeImport { get; set; }
    }
}
