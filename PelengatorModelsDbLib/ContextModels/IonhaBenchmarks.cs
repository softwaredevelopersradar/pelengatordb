﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    public partial class IonhaBenchmarks
    {
        public int Id { get; set; }
        public int Freq { get; set; }
        public int Band { get; set; }
        public double? Azimuth { get; set; }
        public double? Dist { get; set; }
        public int PowerPorog { get; set; }
        public string Destination { get; set; }
    }
}
