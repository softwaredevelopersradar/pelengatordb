﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Runtime.Serialization;

    [DataContract]
    [KnownType(typeof(AbstractModel))]
    [InfoTable(NameTable.Fhss_IRI)]
    public partial class FhssIri : AbstractModel
    {
        [Key]
        public override int Id { get; set; }
        public int IdIri { get; set; }
        public int IdTaskEx { get; set; }
        public double? Azimuth { get; set; }
        public double? AzimuthSko { get; set; }
        public DateTime? TimeB { get; set; }
        public DateTime? TimeE { get; set; }
        public int? Nfreq { get; set; }
        public int? Klass { get; set; }
        public int? Nseans { get; set; }
        public double? Freq { get; set; }
        public double? Band { get; set; }
        public string IdSeanses { get; set; }

        public override object[] GetKey()
        {
            return new object[] { this.IdTaskEx };
        }

        public override void Update(AbstractModel record)
        {
            var newRecord = (FhssIri)record;

            this.Id = newRecord.Id;
            this.IdIri = newRecord.IdIri;
            this.IdTaskEx = newRecord.IdTaskEx;
            this.Azimuth = newRecord.Azimuth;
            this.AzimuthSko = newRecord.AzimuthSko;
            this.TimeB = newRecord.TimeB;
            this.TimeE = newRecord.TimeE;
            this.Nfreq = newRecord.Nfreq;
            this.Klass = newRecord.Klass;
            this.Nseans = newRecord.Nseans;
            this.Freq = newRecord.Freq;
            this.Band = newRecord.Band;
            this.IdSeanses = newRecord.IdSeanses;
        }
    }
}
