﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class Sounds
    {
        public int Id { get; set; }
        public DateTime? Tn { get; set; }
        public double? Freq { get; set; }
        public string Path { get; set; }
    }
}
