﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Runtime.Serialization;

    [DataContract]
    [KnownType(typeof(AbstractModel))]
    [InfoTable(NameTable.ZadachaEmo)]
    public partial class ZadachaEmo : AbstractModel
    {
        [Key]
        public override int Id { get; set; }

        public DateTime? Datecreate { get; set; }

        public int? IdUsercreate { get; set; }

        public string FreqB { get; set; }

        public string FreqE { get; set; }

        public double? Filter { get; set; }

        public DateTime? TimeB { get; set; }

        public DateTime? TimeE { get; set; }

        public double? Porog { get; set; }

        public double? Att { get; set; }

        public int? IdPost { get; set; }

        public int? Status { get; set; }

        public string Name { get; set; }

        public int? IdZadanie { get; set; }

        public string Comment { get; set; }

        public int? DeltaIzm { get; set; }

        public int? IntervalZap { get; set; }

        public int? IdTaskEx { get; set; }

        public int? AzimuthMiddle { get; set; }

        public int? Sector { get; set; }

        public int? Wavetype { get; set; }

        public string Params { get; set; }

        public string TaskUuid { get; set; }

        public int? ReportedStatus { get; set; }

        public int? TaskId { get; set; }

        public string DocUuid { get; set; }

        public string SenderName { get; set; }

        public ZadachaEmo()
        {
            this.Datecreate = DateTime.Now;
            this.TimeB = DateTime.Now;
            this.TimeE = DateTime.Now.AddMinutes(2);
        }

        public ZadachaEmo(
            int id,
            DateTime? datecreate,
            int? idUsercreate,
            byte[] freqB,
            byte[] freqE,
            double? filter,
            DateTime? timeB,
            DateTime? timeE,
            double? porog,
            double? att,
            int? idPost,
            int? status,
            string name,
            int? idZadanie,
            string comment,
            int? deltaIzm,
            int? intervalZap,
            int? idTaskEx,
            int? azimuthMiddle,
            int? sector,
            int? wavetype,
            string @params,
            string taskUuid,
            int? reportedStatus,
            int? taskId,
            string docUuid,
            string senderName)
        {
            this.Id = id;
            this.Datecreate = datecreate;
            this.IdUsercreate = idUsercreate;
            this.FreqB = String.Join(";", freqB);
            this.FreqE = String.Join(";", freqE);
            this.Filter = filter;
            this.TimeB = timeB;
            this.TimeE = timeE;
            this.Porog = porog;
            this.Att = att;
            this.IdPost = idPost;
            this.Status = status;
            this.Name = name;
            this.IdZadanie = idZadanie;
            this.Comment = comment;
            this.DeltaIzm = deltaIzm;
            this.IntervalZap = intervalZap;
            this.IdTaskEx = idTaskEx;
            this.AzimuthMiddle = azimuthMiddle;
            this.Sector = sector;
            this.Wavetype = wavetype;
            this.Params = @params;
            this.TaskUuid = taskUuid;
            this.ReportedStatus = reportedStatus;
            this.TaskId = taskId;
            this.DocUuid = docUuid;
            this.SenderName = senderName;
        }

        public override object[] GetKey()
        {
            return new object[] { this.Id};
        }

        public override void Update(AbstractModel record)
        {
            var newRecord = (ZadachaEmo)record;

            this.Id = newRecord.Id;
            this.Datecreate = newRecord.Datecreate;
            this.IdUsercreate = newRecord.IdUsercreate;
            this.FreqB = newRecord.FreqB;
            this.FreqE = newRecord.FreqB;
            this.Filter = newRecord.Filter;
            this.TimeB = newRecord.TimeB;
            this.TimeE = newRecord.TimeE;
            this.Porog = newRecord.Porog;
            this.Att = newRecord.Att;
            this.IdPost = newRecord.IdPost;
            this.Status = newRecord.Status;
            this.Name = newRecord.Name;
            this.IdZadanie = newRecord.IdZadanie;
            this.Comment = newRecord.Comment;
            this.DeltaIzm = newRecord.DeltaIzm;
            this.IntervalZap = newRecord.IntervalZap;
            this.IdTaskEx = newRecord.IdTaskEx;
            this.AzimuthMiddle = newRecord.AzimuthMiddle;
            this.Sector = newRecord.Sector;
            this.Wavetype = newRecord.Wavetype;
            this.Params = newRecord.Params;
            this.TaskUuid = newRecord.TaskUuid;
            this.ReportedStatus = newRecord.ReportedStatus;
            this.TaskId = newRecord.TaskId;
            this.DocUuid = newRecord.DocUuid;
            this.SenderName = newRecord.SenderName;
        }
    }
}
