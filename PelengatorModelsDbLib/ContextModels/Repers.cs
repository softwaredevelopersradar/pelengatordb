﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    public partial class Repers
    {
        public int Id { get; set; }
        public int? Freq { get; set; }
        public string Mesto { get; set; }
        public double? Lat { get; set; }
        public double? Lon { get; set; }
        public string Desc { get; set; }
        public string Klass { get; set; }
        public string Scc { get; set; }
        public string Worktime { get; set; }
        public string Url { get; set; }
        public string Prim { get; set; }
        public int? Width { get; set; }
    }
}
