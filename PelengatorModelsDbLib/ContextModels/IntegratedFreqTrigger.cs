﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class IntegratedFreqTrigger
    {
        public int Id { get; set; }
        public int? IdIntegratedFreqRecords { get; set; }
        public DateTime? TimeB { get; set; }
        public int? IdServersFinder { get; set; }
        public int? NumCalc { get; set; }
    }
}
