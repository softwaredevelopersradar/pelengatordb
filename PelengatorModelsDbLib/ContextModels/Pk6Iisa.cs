﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class Pk6Iisa
    {
        public int Id { get; set; }
        public DateTime Time { get; set; }
        public double LatSt { get; set; }
        public double LonSt { get; set; }
        public string NameSt { get; set; }
        public double? Wolf { get; set; }
        public double? Wtec { get; set; }
    }
}
