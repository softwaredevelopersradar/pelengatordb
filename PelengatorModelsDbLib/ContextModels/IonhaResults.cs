﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class IonhaResults
    {
        public int Id { get; set; }
        public int? Ind { get; set; }
        public DateTime? Time { get; set; }
        public double? SlopFreq { get; set; }
        public double? VertFreq { get; set; }
        public int? TypeModel { get; set; }
        public int? W { get; set; }
    }
}
