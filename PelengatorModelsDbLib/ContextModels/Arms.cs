﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    public partial class Arms
    {
        public int Id { get; set; }
        public int? TypePk { get; set; }
        public string NamePk { get; set; }
        public string MacPk { get; set; }
    }
}
