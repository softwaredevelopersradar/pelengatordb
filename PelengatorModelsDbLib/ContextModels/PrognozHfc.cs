﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    public partial class PrognozHfc
    {
        public int? IdZondResults { get; set; }
        public double? F { get; set; }
        public double? H { get; set; }
        public int? M { get; set; }
        public int Ind { get; set; }
    }
}
