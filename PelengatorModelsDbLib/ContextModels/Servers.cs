﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace PelengatorModelsDbLib.ContextModels
{
    [DataContract]
    [KnownType(typeof(AbstractModel))]
    [InfoTable(NameTable.Servers)]
    public partial class Servers : AbstractModel
    {
        [Key]
        public override int Id { get; set; }
        public string Name { get; set; }
        public int TypePk { get; set; }
        public int TypeServer { get; set; }
        public string Ip { get; set; }
        public string PortUnp { get; set; }
        public string PortBin { get; set; }
        public string PortSnd { get; set; }
        public string TimeOut { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public double? Lat { get; set; }
        public double? Lon { get; set; }
        public int? Color { get; set; }
        public string Mesto { get; set; }
        public string AddDescript { get; set; }
        public double? FreqB { get; set; }
        public double? FreqE { get; set; }
        public int? Index { get; set; }
        public int? Default { get; set; }

        public override object[] GetKey()
        {
            return new object[] { this.Id };
        }

        public override void Update(AbstractModel record)
        {
            var newRecord = (Servers)record;

            this.Id = newRecord.Id;
            this.Name = newRecord.Name;
            this.TypePk = newRecord.TypePk;
            this.TypeServer = newRecord.TypeServer;
            this.Ip = newRecord.Ip;
            this.PortUnp = newRecord.PortUnp;
            this.PortBin = newRecord.PortBin;
            this.PortSnd = newRecord.PortSnd;
            this.TimeOut = newRecord.TimeOut;
            this.Login = newRecord.Login;
            this.Password = newRecord.Password;
            this.Mesto = newRecord.Mesto;
            this.AddDescript = newRecord.AddDescript;
        }
    }
}
