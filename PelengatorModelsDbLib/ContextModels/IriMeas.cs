﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class IriMeas
    {
        public int Id { get; set; }
        public int IdIri { get; set; }
        public int? IdTaskEx { get; set; }
        public double? Freq { get; set; }
        public double? Shirina { get; set; }
        public double? FreqLSh { get; set; }
        public double? FreqRSh { get; set; }
        public double? USh { get; set; }
        public double? Raznos { get; set; }
        public double? FreqLRaz { get; set; }
        public double? FreqRRaz { get; set; }
        public string Klass { get; set; }
        public int? NumPoz { get; set; }
        public int? RaznosPoz { get; set; }
        public double? Skor { get; set; }
        public double? U { get; set; }
        public double? E { get; set; }
        public double? FreqSp { get; set; }
        public double? Step { get; set; }
        public byte[] Spectr { get; set; }
        public DateTime? TimeB { get; set; }
        public DateTime? TimeE { get; set; }
        public int? NumIzm { get; set; }
        public double? FreqD { get; set; }
        public byte[] Iq { get; set; }
        public string IqPath { get; set; }
        public string Scc { get; set; }
    }
}
