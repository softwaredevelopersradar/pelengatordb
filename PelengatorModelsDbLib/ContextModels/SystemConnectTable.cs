﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class SystemConnectTable
    {
        public int Id { get; set; }
        public string Ip { get; set; }
        public byte[] IpBin { get; set; }
        public string NamePc { get; set; }
        public DateTime? TimeB { get; set; }
        public DateTime? TimeE { get; set; }
        public string Description { get; set; }
        public int StatusLic { get; set; }
    }
}
