﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Runtime.Serialization;

    [DataContract]
    [KnownType(typeof(AbstractModel))]
    [InfoTable(NameTable.Fhss_IRI_Loacation)]
    public partial class FhssIriLocation : AbstractModel
    {
        [Key]
        public override int Id { get; set; }
        public int? IdIri { get; set; }
        public int? IdTaskEx { get; set; }
        public DateTime? PositioningTime { get; set; }
        public double? Lat { get; set; }
        public double? Lon { get; set; }
        public double? A { get; set; }
        public double? B { get; set; }
        public double? Alpha { get; set; }
        public string AllDist { get; set; }

        public override object[] GetKey()
        {
            return new object[] { this.IdTaskEx };
        }

        public override void Update(AbstractModel record)
        {
            var newRecord = (FhssIriLocation)record;

            this.Id = newRecord.Id;
            this.IdIri = newRecord.IdIri;
            this.IdTaskEx = newRecord.IdTaskEx;
            this.PositioningTime = newRecord.PositioningTime;
            this.Lat = newRecord.Lat;
            this.Lon = newRecord.Lon;
            this.A = newRecord.A;
            this.B = newRecord.B;
            this.Alpha = newRecord.Alpha;
            this.AllDist = newRecord.AllDist;
        }
    }
}
