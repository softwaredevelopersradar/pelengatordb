﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class AfsComplex
    {
        public int? TypeComplex { get; set; }
        public string X { get; set; }
        public string Y { get; set; }
        public string Ze { get; set; }
        public double? KursAngle { get; set; }
        public double? KursAngleRamka { get; set; }
        public double? Syserror { get; set; }
        public int? TypeAfs { get; set; }
        public string Gr1 { get; set; }
        public string Gr2 { get; set; }
        public string Gr3 { get; set; }
        public double? Lat { get; set; }
        public double? Lon { get; set; }
        public int? InverPh { get; set; }
        public DateTime? Dt { get; set; }
    }
}
