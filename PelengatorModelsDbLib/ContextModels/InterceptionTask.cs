﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class InterceptionTask
    {
        public int Id { get; set; }
        public int? IdTaskEx { get; set; }
        public double? Freq { get; set; }
        public double? Azimuth { get; set; }
        public double? Ugm { get; set; }
        public DateTime? TimeB { get; set; }
        public DateTime? TimeE { get; set; }
        public string Klass { get; set; }
        public string Scc { get; set; }
        public int? Mode { get; set; }
        public int? RecTime { get; set; }
        public int? PreRecTime { get; set; }
        public int? LossRecTime { get; set; }
        public int? DetectTime { get; set; }
        public int? BanTime { get; set; }
        public int? Status { get; set; }
        public int? InterceptStatus { get; set; }
        public DateTime? RealTime { get; set; }
        public string AzimuthMiddle { get; set; }
        public string Sector { get; set; }
        public DateTime? TimeBan { get; set; }
    }
}
