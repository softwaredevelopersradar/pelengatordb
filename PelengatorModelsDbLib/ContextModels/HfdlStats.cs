﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class HfdlStats
    {
        public int Id { get; set; }
        public double? Freq { get; set; }
        public DateTime? TimeB { get; set; }
        public DateTime? TimeE { get; set; }
        public int? TypeStat { get; set; }
        public double? AzimuthMid { get; set; }
        public double? AzimuthSko { get; set; }
        public int? AzimuthCntFreq { get; set; }
        public int? AzimuthCntSeans { get; set; }
        public double? TizSko { get; set; }
        public int? TizCntFreq { get; set; }
        public int? TizCntSeans { get; set; }
        public double? IzmSko { get; set; }
        public int? IzmCntFreq { get; set; }
        public int? IzmCntSeans { get; set; }
        public double? IonhaSko { get; set; }
        public int? IonhaCntFreq { get; set; }
        public int? IonhaCntSeans { get; set; }
    }
}
