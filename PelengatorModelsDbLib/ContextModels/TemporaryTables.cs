﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    public partial class TemporaryTables
    {
        public int Id { get; set; }
        public int IdServers { get; set; }
        public int IdTaskEx { get; set; }
        public int Status { get; set; }
    }
}
