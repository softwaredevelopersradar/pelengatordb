﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class Result
    {
        public int Id { get; set; }
        public int? TypePk { get; set; }
        public string Post { get; set; }
        public string ModulRpu { get; set; }
        public int? Cym { get; set; }
        public double? FreqRpu { get; set; }
        public double? FreqSig { get; set; }
        public int? DeltaFr { get; set; }
        public DateTime? Tn { get; set; }
        public DateTime? Tk { get; set; }
        public double? DeltaT { get; set; }
        public double? U { get; set; }
        public double? Shir { get; set; }
        public double? Deviation { get; set; }
        public double? Lat { get; set; }
        public double? Lon { get; set; }
        public string Adress { get; set; }
        public int? N { get; set; }
        public string Callsign { get; set; }
        public string Vladelec { get; set; }
        public string Mesto { get; set; }
        public string CallSt { get; set; }
        public string Protokol { get; set; }
        public string TypePr { get; set; }
        public int? TaskExId { get; set; }
        public int? TaskExFixId { get; set; }
        public string Prim { get; set; }
        public string SoundPath { get; set; }
        public int? StationId { get; set; }
        public int? Porog { get; set; }
        public int? Status { get; set; }
        public double? Azimuth { get; set; }
        public double? Ugm { get; set; }
        public double? Daln { get; set; }
        public string Klass { get; set; }
        public double? Skor { get; set; }
        public double? UShir { get; set; }
        public double? OwnLat { get; set; }
        public double? OwnLon { get; set; }
        public int? IdMPel { get; set; }
        public string Radionet { get; set; }
        public int? IdUser { get; set; }
        public string Callfrom { get; set; }
        public string Callto { get; set; }
        public string SendProtocol { get; set; }
        public string PathFile { get; set; }
        public string Radiochange { get; set; }
        public string FuncMode { get; set; }
        public string Depart { get; set; }
        public string State { get; set; }
    }
}
