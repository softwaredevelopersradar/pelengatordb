﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class FbPanorama
    {
        public int Id { get; set; }
        public int IdTaskEx { get; set; }
        public DateTime? TimeB { get; set; }
        public double? Lat { get; set; }
        public double? Lon { get; set; }
        public byte[] Powers { get; set; }
        public byte[] Azimuth { get; set; }
        public byte[] Ugm { get; set; }
        public double? FreqB { get; set; }
        public int? FreqStep { get; set; }
        public int? TimeBSec { get; set; }
        public string AzimuthText { get; set; }
    }
}
