﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    public partial class HfdlLpfCut
    {
        public int Id { get; set; }
        public double? Lat { get; set; }
        public double? Lon { get; set; }
        public double? A { get; set; }
        public double? B { get; set; }
        public double? Alpha { get; set; }
        public int? CoordFound { get; set; }
    }
}
