﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class Interception
    {
        public int Id { get; set; }
        public int IdSignal { get; set; }
        public int? IdInterceptTask { get; set; }
        public int? IdTaskEx { get; set; }
        public double? Freq { get; set; }
        public DateTime? TimeB { get; set; }
        public DateTime? TimeE { get; set; }
        public int? DecodType { get; set; }
        public string Params { get; set; }
        public int? Status { get; set; }
        public string SignalPath { get; set; }
        public string DecodPath { get; set; }
        public int? ResText { get; set; }
        public int? ResBin { get; set; }
        public int? ResSemantic { get; set; }
        public string TextPath { get; set; }
        public string BinPath { get; set; }
        public string SemanticPath { get; set; }
        public string Scc { get; set; }
        public DateTime? RealTime { get; set; }
        public string CallsignFrom { get; set; }
        public string CallsignTo { get; set; }
        public string SeansUgm { get; set; }
        public string SeansAz { get; set; }
        public string SeansB { get; set; }
        public string SeansE { get; set; }
        public string SeansScc { get; set; }
        public string SeansKlass { get; set; }
        public string SeansD { get; set; }
        public int? IqwTime { get; set; }
        public int? TextSize { get; set; }
        public int? BinSize { get; set; }
        public int? SemSize { get; set; }

        public virtual InterceptionTask IdInterceptTaskNavigation { get; set; }
    }
}
