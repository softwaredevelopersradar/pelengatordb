﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Runtime.Serialization;

    [DataContract]
    [KnownType(typeof(AbstractModel))]
    [InfoTable(NameTable.Fhss_Seans)]
    public partial class FhssSeans : AbstractModel
    {
        [Key]
        public override int Id { get; set; }
        public int IdSeans { get; set; }
        public int IdIri { get; set; }
        public int IdTaskEx { get; set; }
        public double Azimuth { get; set; }
        public double? Sko { get; set; }
        public DateTime? TimeB { get; set; }
        public DateTime? TimeE { get; set; }
        public int? Nfreq { get; set; }
        public int? Klass { get; set; }
        public int? NAllFreq { get; set; }
        public string Freqs { get; set; }
        public string Bounds { get; set; }
        public string AllFreqs { get; set; }
        public string AllBounds { get; set; }
        public string AllAzimuths { get; set; }
        public string AllUgm { get; set; }
        public string AllTimeB { get; set; }
        public string AllTimeE { get; set; }
        public string Waveform { get; set; }
        public double? TPos { get; set; }
        public double? TPosSko { get; set; }
        public double? PosBandMax { get; set; }
        public double? PosBandAv { get; set; }
        public double? PosBandSko { get; set; }
        public double? PosBySec { get; set; }

        public override object[] GetKey()
        {
            return new object[] { this.IdTaskEx };
        }

        public override void Update(AbstractModel record)
        {
            var newRecord = (FhssSeans)record;

            this.Id = newRecord.Id;
            this.IdSeans = newRecord.IdSeans;
            this.IdIri = newRecord.IdIri;
            this.IdTaskEx = newRecord.IdTaskEx;
            this.Azimuth = newRecord.Azimuth;
            this.Sko = newRecord.Sko;
            this.TimeB = newRecord.TimeB;
            this.TimeE = newRecord.TimeE;
            this.Nfreq = newRecord.Nfreq;
            this.Klass = newRecord.Klass;
            this.NAllFreq = newRecord.NAllFreq;
            this.Freqs = newRecord.Freqs;
            this.Bounds = newRecord.Bounds;
            this.AllFreqs = newRecord.AllFreqs;
            this.AllBounds = newRecord.AllBounds;
            this.AllAzimuths = newRecord.AllAzimuths;
            this.AllUgm = newRecord.AllUgm;
            this.AllTimeB = newRecord.AllTimeB;
            this.AllTimeE = newRecord.AllTimeE;
            this.Waveform = newRecord.Waveform;
            this.TPos = newRecord.TPos;
            this.TPosSko = newRecord.TPosSko;
            this.PosBandMax = newRecord.PosBandMax;
            this.PosBandAv = newRecord.PosBandAv;
            this.PosBandSko = newRecord.PosBandSko;
            this.PosBySec = newRecord.PosBySec;
        }
    }
}
