﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class IntegratedFreqRecords
    {
        public int Id { get; set; }
        public double? CentralFreq { get; set; }
        public DateTime? TimeB { get; set; }
        public DateTime? TimeE { get; set; }
        public double? DeltaT { get; set; }
        public double? Azimuth { get; set; }
        public double? SkoAzimuth { get; set; }
        public double? Ugm { get; set; }
        public double? SkoUgm { get; set; }
        public double? Polosa { get; set; }
        public double? U { get; set; }
        public int? Skor { get; set; }
        public int? RaznosPoz { get; set; }
        public int? NumPoz { get; set; }
        public int? Wavetype { get; set; }
        public int? NumSeans { get; set; }
        public string IdsIntegratedKlass { get; set; }
        public string IdsIntegratedScc { get; set; }
        public string IdsServerFinder { get; set; }
        public DateTime? AlterTime { get; set; }
        public int AlterType { get; set; }
        public int FreqIntegrated { get; set; }
        public int? Activ { get; set; }
        public string WeightKlass { get; set; }
        public int? InterceptStatus { get; set; }
    }
}
