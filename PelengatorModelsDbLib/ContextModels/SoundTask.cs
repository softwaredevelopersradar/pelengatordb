﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class SoundTask
    {
        public int Id { get; set; }
        public double? Freq { get; set; }
        public DateTime? TimeB { get; set; }
        public DateTime? TimeE { get; set; }
        public string ModeRpu { get; set; }
        public string Format { get; set; }
        public int? Status { get; set; }
        public double? U { get; set; }
        public string Path { get; set; }
    }
}
