﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class Temp12825e2df1d726b1efPk4
    {
        public int Ind { get; set; }
        public string IdServers { get; set; }
        public int? IdIntegratedFreqRecords { get; set; }
        public double? CentralFreq { get; set; }
        public double? Azimuth { get; set; }
        public double? SkoAzimuth { get; set; }
        public double? Ugm { get; set; }
        public double? SkoUgm { get; set; }
        public double? Polosa { get; set; }
        public double? U { get; set; }
        public int? Skor { get; set; }
        public int? RaznosPoz { get; set; }
        public int? NumPoz { get; set; }
        public string Klass { get; set; }
        public string KlassWeights { get; set; }
        public string Scc { get; set; }
        public DateTime? TimeB { get; set; }
        public DateTime? TimeE { get; set; }
        public double? DeltaT { get; set; }
        public int? Wavetype { get; set; }
        public string Network { get; set; }
        public string Protocol { get; set; }
        public string ClientFrom { get; set; }
        public string ClientTo { get; set; }
        public string FilesInfo { get; set; }
        public double? Lat { get; set; }
        public double? Lon { get; set; }
        public string AllDist { get; set; }
        public int? NumSeans { get; set; }
        public int? Activ { get; set; }
        public int? AlterType { get; set; }
        public int? LoadPercent { get; set; }
    }
}
