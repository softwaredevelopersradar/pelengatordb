﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class HfdlLpfResults
    {
        public int Id { get; set; }
        public int IdHfdlLdfServers { get; set; }
        public int IdHfdlLpfCut { get; set; }
        public double? Freq { get; set; }
        public int? Icao { get; set; }
        public string Flight { get; set; }
        public DateTime? TimeMessage { get; set; }
        public double? Lat { get; set; }
        public double? Lon { get; set; }
        public DateTime? TimeRecept { get; set; }
        public string Register { get; set; }
        public string Crc { get; set; }
        public string CrcOk { get; set; }
        public double? Azimuth { get; set; }
        public double? AzimuthPel { get; set; }
        public double? Error { get; set; }
        public double? Dist { get; set; }
        public double? SkoAz { get; set; }
        public double? DistCut { get; set; }
        public double? ErrorDistCut { get; set; }
    }
}
