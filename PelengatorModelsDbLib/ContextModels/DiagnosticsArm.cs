﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class DiagnosticsArm
    {
        public int Id { get; set; }
        public int NumArm { get; set; }
        public int TypePk { get; set; }
        public int Status { get; set; }
        public DateTime Time { get; set; }
        public int? Unixtime { get; set; }
    }
}
