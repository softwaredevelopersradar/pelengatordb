﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class FixfreqResult
    {
        public int Id { get; set; }
        public double? Freq { get; set; }
        public DateTime? TimeB { get; set; }
        public string Demod { get; set; }
        public int? IdZadachaFix { get; set; }
        public byte[] Sound { get; set; }
        public int? FileType { get; set; }
        public string FilePath { get; set; }
    }
}
