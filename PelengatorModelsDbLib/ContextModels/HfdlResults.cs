﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class HfdlResults
    {
        public int Id { get; set; }
        public double? Freq { get; set; }
        public int? Icao { get; set; }
        public string Flight { get; set; }
        public DateTime? TimeMessage { get; set; }
        public double? Lat { get; set; }
        public double? Lon { get; set; }
        public DateTime? TimeRecept { get; set; }
        public string Register { get; set; }
        public string Crc { get; set; }
        public string CrcOk { get; set; }
        public double? Azimuth { get; set; }
        public double? AzimuthPel { get; set; }
        public double? Error { get; set; }
        public double? Dist { get; set; }
        public double? Ugm { get; set; }
        public double? U { get; set; }
        public double? DistTiz { get; set; }
        public double? ErrorTiz { get; set; }
        public double? DistIzmiran { get; set; }
        public double? ErrorIzmiran { get; set; }
        public double? DistIonha { get; set; }
        public double? ErrorIonha { get; set; }
        public double? DistTiz2 { get; set; }
        public double? ErrorTiz2 { get; set; }
        public double? SkoAz { get; set; }
        public double? SkoUgm { get; set; }
    }
}
