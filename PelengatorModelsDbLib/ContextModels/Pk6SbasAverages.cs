﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class Pk6SbasAverages
    {
        public int Id { get; set; }
        public DateTime? Time { get; set; }
        public int? Hour { get; set; }
        public double? Lat { get; set; }
        public double? Lon { get; set; }
        public double? Iisa { get; set; }
        public double? F0F2 { get; set; }
    }
}
