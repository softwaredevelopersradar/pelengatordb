﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class ManualPel
    {
        public int Id { get; set; }
        public double? Freq { get; set; }
        public double? Band { get; set; }
        public double? Azimuth { get; set; }
        public DateTime? TimeB { get; set; }
        public DateTime? TimeE { get; set; }
        public int? Status { get; set; }
        public int? TypePk { get; set; }
        public int? IdCom { get; set; }
    }
}
