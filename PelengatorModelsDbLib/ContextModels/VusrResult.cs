﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class VusrResult
    {
        public int Id { get; set; }
        public int Freq { get; set; }
        public DateTime? TimeB { get; set; }
        public DateTime? TimeE { get; set; }
        public string IdDetect { get; set; }
        public double? Azimuth { get; set; }
        public string WavPath { get; set; }
        public string DecPath { get; set; }
        public string ProtPath { get; set; }
        public string AzArray { get; set; }
    }
}
