﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;
    using System.Collections.Generic;

    public partial class Benchmarks
    {
        public Benchmarks()
        {
            this.DopplerDeniedMeas = new HashSet<DopplerDeniedMeas>();
            this.DopplerEvents = new HashSet<DopplerEvents>();
            this.DopplerMeas = new HashSet<DopplerMeas>();
        }

        public int Id { get; set; }
        public int Freq { get; set; }
        public int Baseband { get; set; }
        public float Lat { get; set; }
        public float Lon { get; set; }
        public int PowerPorog { get; set; }
        public bool? PowerPorogNoise { get; set; }
        public float AzimuthPorog { get; set; }
        public int DopplerPorog { get; set; }
        public string ClassId { get; set; }
        public string Description { get; set; }
        public bool? Active { get; set; }
        public int? MeasFreq { get; set; }
        public int? MeasPower { get; set; }
        public bool? Doppler { get; set; }
        public DateTime? MeasTime { get; set; }
        public int? MeasUnixtime { get; set; }
        public double? Azimuth { get; set; }
        public double? Dist { get; set; }
        public string Klass { get; set; }

        public virtual ICollection<DopplerDeniedMeas> DopplerDeniedMeas { get; set; }
        public virtual ICollection<DopplerEvents> DopplerEvents { get; set; }
        public virtual ICollection<DopplerMeas> DopplerMeas { get; set; }
    }
}
