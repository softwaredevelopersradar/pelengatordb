﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class Eventhistory
    {
        public long EventhistoryId { get; set; }
        public string EventhistoryTableName { get; set; }
        public long? EventhistoryDocNumber { get; set; }
        public string EventhistoryEventName { get; set; }
        public DateTime? EventhistoryDt { get; set; }
        public int? EventhistoryProcessed { get; set; }
        public int? ReceiverId { get; set; }
        public int? SenderId { get; set; }
        public int? PersonalsId { get; set; }
    }
}
