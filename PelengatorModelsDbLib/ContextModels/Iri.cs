﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class Iri
    {
        public int Id { get; set; }
        public int? IdIri { get; set; }
        public int? IdNetwork { get; set; }
        public double? Freq { get; set; }
        public double? Azimuth { get; set; }
        public double? Sko { get; set; }
        public double? Polosa { get; set; }
        public double? U { get; set; }
        public DateTime? TimeB { get; set; }
        public DateTime? TimeE { get; set; }
        public double? DeltaT { get; set; }
        public double? AzimuthB { get; set; }
        public double? AzimuthE { get; set; }
        public int? Wavetype { get; set; }
        public double? Dist { get; set; }
        public int IdTaskEx { get; set; }
        public int? Ind { get; set; }
        public int? IdIriKlass { get; set; }
        public int? IdRealNetwork { get; set; }
        public string AllDist { get; set; }
    }
}
