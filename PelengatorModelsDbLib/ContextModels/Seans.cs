﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class Seans
    {
        public int Id { get; set; }
        public int? IdSeans { get; set; }
        public int? IdIri { get; set; }
        public double? Ugm { get; set; }
        public double? Sko { get; set; }
        public DateTime? TimeB { get; set; }
        public DateTime? TimeE { get; set; }
        public int IdTaskEx { get; set; }
        public double? Azimuth { get; set; }
        public double? Skoa { get; set; }
    }
}
