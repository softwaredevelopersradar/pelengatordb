﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Runtime.Serialization;

    [DataContract]
    [KnownType(typeof(AbstractModel))]
    [InfoTable(NameTable.Diagnistics)]
    public partial class Diagnostics : AbstractModel
    {
        public Diagnostics()
        {
            this.DopplerDeniedMeas = new HashSet<DopplerDeniedMeas>();
            this.DopplerMeas = new HashSet<DopplerMeas>();
        }

        [Key]
        public override int Id { get; set; }

        public int IdServers { get; set; }
        public int TypeDiagObj { get; set; }
        public int Status { get; set; }
        public DateTime Time { get; set; }
        public int? Unixtime { get; set; }

        public virtual ICollection<DopplerDeniedMeas> DopplerDeniedMeas { get; set; }
        public virtual ICollection<DopplerMeas> DopplerMeas { get; set; }

        public Diagnostics(int id, int idServers, int typeDiagObj, int status, DateTime time = default, int? unixtime = null)
        {
            Id = id;
            IdServers = idServers;
            TypeDiagObj = typeDiagObj;
            Status = status;
            Time = time;
            Unixtime = unixtime;
        }

        public override object[] GetKey()
        {
            return new object[] { this.Id };
        }

        public override void Update(AbstractModel record)
        {
            var newRecord = (Diagnostics)record;

            this.Id = newRecord.Id;
            this.IdServers = newRecord.IdServers;
            this.TypeDiagObj = newRecord.TypeDiagObj;
            this.Status = newRecord.Status;
            this.Time = newRecord.Time;
            this.Unixtime = newRecord.Unixtime;
        }
    }
}

