﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class Panorama
    {
        public int Id { get; set; }
        public int? IdTaskEx { get; set; }
        public int? IdTaskExDiap { get; set; }
        public DateTime? TimeB { get; set; }
        public DateTime? TimeE { get; set; }
        public byte[] Spectr { get; set; }
        public int? TimeBMs { get; set; }
        public int? TimeEMs { get; set; }
    }
}
