﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    public partial class IntegratedKlass
    {
        public int Id { get; set; }
        public string Klass { get; set; }
        public int? Checksum { get; set; }
    }
}
