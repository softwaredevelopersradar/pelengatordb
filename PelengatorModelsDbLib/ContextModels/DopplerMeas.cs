﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;
    using System.Collections.Generic;

    public partial class DopplerMeas
    {
        public DopplerMeas()
        {
            this.DopplerEvents = new HashSet<DopplerEvents>();
        }

        public int Id { get; set; }
        public int BenchmarkId { get; set; }
        public int DiagnosticsId { get; set; }
        public int Power { get; set; }
        public int Shift { get; set; }
        public float? Azimuth { get; set; }
        public float? Angle { get; set; }
        public DateTime Time { get; set; }
        public int Unixtime { get; set; }

        public virtual Benchmarks Benchmark { get; set; }
        public virtual Diagnostics Diagnostics { get; set; }
        public virtual ICollection<DopplerEvents> DopplerEvents { get; set; }
    }
}
