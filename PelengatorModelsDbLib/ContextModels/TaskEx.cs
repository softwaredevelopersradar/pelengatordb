﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class TaskEx
    {
        public int Id { get; set; }
        public int? Type { get; set; }
        public double? FreqB { get; set; }
        public double? FreqE { get; set; }
        public double? Step { get; set; }
        public string Rpu { get; set; }
        public int? Att { get; set; }
        public int? Tresh { get; set; }
        public DateTime? TimeB { get; set; }
        public DateTime? TimeE { get; set; }
        public int? PrtiId { get; set; }
        public string Operator { get; set; }
        public int? TaskEmoId { get; set; }
        public string Path { get; set; }
        public double? Lat { get; set; }
        public double? Lon { get; set; }
    }
}
