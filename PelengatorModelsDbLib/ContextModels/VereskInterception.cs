﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class VereskInterception
    {
        public int Id { get; set; }
        public int IdQuery { get; set; }
        public int? IdTaskEx { get; set; }
        public int IdVTsignal { get; set; }
        public string Type { get; set; }
        public DateTime RealTime { get; set; }
        public string IdLoid { get; set; }
    }
}
