﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class Station
    {
        public string Vid { get; set; }
        public string TipRes { get; set; }
        public string Vladelec { get; set; }
        public string Adres { get; set; }
        public string Mesto { get; set; }
        public string Vd { get; set; }
        public string Ss { get; set; }
        public double? P { get; set; }
        public double? Fi { get; set; }
        public double? Fp { get; set; }
        public string Klass { get; set; }
        public string Pozyvnoj { get; set; }
        public double? H { get; set; }
        public string Razresch { get; set; }
        public DateTime? Data { get; set; }
        public DateTime? Srok { get; set; }
        public string SvReg { get; set; }
        public DateTime? DataSvR { get; set; }
        public DateTime? SrokSvR { get; set; }
        public string RazrFreq { get; set; }
        public DateTime? DataRFr { get; set; }
        public DateTime? SrokRFr { get; set; }
        public string Zavnomer { get; set; }
        public string Centrede { get; set; }
        public string Func { get; set; }
        public double? Azimut { get; set; }
        public double? Usilenie { get; set; }
        public double? Uroven { get; set; }
        public string Antenna { get; set; }
        public string Polariz { get; set; }
        public string Otvetst { get; set; }
        public int Ind { get; set; }
        public double? Vd1 { get; set; }
        public double? Ss1 { get; set; }
    }
}
