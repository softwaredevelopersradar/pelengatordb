﻿

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PelengatorModelsDbLib.ContextModels
{
    using System;

    public partial class Pk6Satellites
    {
        public int Id { get; set; }
        public DateTime Time { get; set; }
        public double Elevation { get; set; }
        public double Azimuth { get; set; }
        public double Sn { get; set; }
        public int Sys { get; set; }
        public long? Sat { get; set; }
        public double? L1 { get; set; }
        public double? L2 { get; set; }
    }
}
