﻿using System.Runtime.Serialization;

namespace PelengatorModelsDbLib
{
    [DataContract]
    public abstract class AbstractModel
    {
        [DataMember]
        public abstract int Id { get; set; }

        public abstract object[] GetKey();

        public abstract void Update(AbstractModel record);
    }
}
