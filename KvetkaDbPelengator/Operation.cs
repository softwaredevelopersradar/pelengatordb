﻿using Microsoft.EntityFrameworkCore;
using PelengatorModelsDbLib;

namespace KvetkaDbPelengator
{
    using PelengatorModelsDbLib.Context;
    using System.Net;

    public class Operation
    {
        protected static G81Context DataBase;

        protected NameTable Name;

        public Operation()
        {
            if (DataBase != null) return;
            DataBase = new G81Context(this.GetConfig());
        }

        public Operation(IPAddress address = default,
                         string databaseName = "G81",
                         string login = "g81",
                         string password = "G81")
        {
            if (DataBase != null) return;
            DataBase = new G81Context(this.GetConfig(address, databaseName, login, password));
        }

        private DbContextOptions<G81Context> GetConfig(
            IPAddress address = default,
            string databaseName = "G81",
            string login = "g81",
            string password = "G81")
        {
            if (address == null || address.Equals(default))
            {
                address = IPAddress.Parse("192.168.1.120");
            }
            // получаем строку подключения
            string connectionString =
                $"Server={address.ToString()};Database={databaseName};User Id={login};Password={password};";

            var optionsBuilder = new DbContextOptionsBuilder<G81Context>();
            var options = optionsBuilder.UseSqlServer(connectionString).Options;

            return options;
        }
        
    }
}
