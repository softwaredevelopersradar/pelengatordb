﻿using Microsoft.EntityFrameworkCore;
using PelengatorModelsDbLib;
using PelengatorModelsDbLib.ContextModels;
using System;
using System.Linq;
using System.Net;

namespace KvetkaDbPelengator
{
    using PelengatorModelsDbLib.Context;
    using PelengatorModelsDbLib.ModelsForServer;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;

    public class OperationTableFhssIri<T> : OperationTable<FhssIri>
    {
        public OperationTableFhssIri()
        {
            object[] attrs = typeof(T).GetCustomAttributes(typeof(InfoTableAttribute), false);
            foreach (InfoTableAttribute info in attrs)
            {
                Name = info.Name;
            }
        }

        public OperationTableFhssIri(
            IPAddress address = default,
            string databaseName = "G81",
            string login = "g81",
            string password = "G81")
        {
            object[] attrs = typeof(T).GetCustomAttributes(typeof(InfoTableAttribute), false);
            foreach (InfoTableAttribute info in attrs)
            {
                Name = info.Name;
            }
        }

        public override ClassDataCommon Load(int id)
        {
            try
            {
                using (G81Context g81 = new G81Context())
                {
                    DbSet<FhssIri> Table = g81.GetTable<FhssIri>(Name);
                    var result = Table.Where(t => t.IdTaskEx == id).ToList();
                    if (result.Count != 0)
                    {
                        List<FhssIriServer> ServerList = new List<FhssIriServer>();
                        foreach (var server in result)
                        {
                            var temp = new FhssIriServer();
                            temp.Update(server);
                            ServerList.Add(temp);
                        }
                        return ClassDataCommon.ConvertToListAbstractCommonTable(ServerList);
                    }
                    return ClassDataCommon.ConvertToListAbstractCommonTable(result);
                }
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new ExceptionLocalDB(exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(ex.Message);
            }
        }

        /// <inheritdoc />
        public override async Task<ClassDataCommon> LoadAsync(int id, CancellationToken token = default)
        {
            try
            {
                using (G81Context g81 = new G81Context())
                {
                    DbSet<FhssIri> Table = g81.GetTable<FhssIri>(Name);
                    await g81.Entry(Table.OrderByDescending(p => p.IdTaskEx).FirstOrDefault()).ReloadAsync();
                    var result = Table.Where(t => t.IdTaskEx == id).ToList();
                    if (result.Count != 0)
                    {
                        List<FhssIriServer> ServerList = new List<FhssIriServer>();
                        foreach (var server in result)
                        {
                            var temp = new FhssIriServer();
                            temp.Update(server);
                            ServerList.Add(temp);
                        }
                        return ClassDataCommon.ConvertToListAbstractCommonTable(ServerList);
                    }
                    return ClassDataCommon.ConvertToListAbstractCommonTable(result);
                }
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new ExceptionLocalDB(exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(ex.Message);
            }
        }
    }
}
