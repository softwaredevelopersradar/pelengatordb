﻿using PelengatorModelsDbLib;
using System.Threading.Tasks;

namespace KvetkaDbPelengator
{
    using System.Threading;

    public interface ITableAction
    {

        bool Add(AbstractModel record);

        Task<bool> AddAsync(AbstractModel record, CancellationToken token = default);

        ClassDataCommon Load(int id);

        Task<ClassDataCommon> LoadAsync(int id, CancellationToken token = default);

        bool Change(AbstractModel record);

        Task<bool> ChangeAsync(AbstractModel record, CancellationToken token = default);
    }
}
