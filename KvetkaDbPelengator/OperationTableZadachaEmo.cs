﻿using Microsoft.EntityFrameworkCore;
using PelengatorModelsDbLib;
using PelengatorModelsDbLib.ContextModels;
using System;
using System.Linq;
using System.Net;
using Microsoft.EntityFrameworkCore.Internal;

namespace KvetkaDbPelengator
{
    using PelengatorModelsDbLib.Context;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    public class OperationTableZadachaEmo<T> : OperationTable<ZadachaEmo>
    {
        public OperationTableZadachaEmo()
        {
            object[] attrs = typeof(T).GetCustomAttributes(typeof(InfoTableAttribute), false);
            foreach (InfoTableAttribute info in attrs)
            {
                Name = info.Name;
            }
        }

        public OperationTableZadachaEmo(
            IPAddress address = default,
            string databaseName = "G81",
            string login = "g81",
            string password = "G81")
        {
            object[] attrs = typeof(T).GetCustomAttributes(typeof(InfoTableAttribute), false);
            foreach (InfoTableAttribute info in attrs)
            {
                Name = info.Name;
            }
        }

        public override ClassDataCommon Load(int id)
        {
            try
            {
                using (G81Context g81 = new G81Context())
                {
                    DbSet<ZadachaEmo> Table = g81.GetTable<ZadachaEmo>(Name);
                    var lastIdTask = Table.OrderByDescending(p => p.IdTaskEx).FirstOrDefault();
                    return ClassDataCommon.ConvertToListAbstractCommonTable(new List<ZadachaEmo> { lastIdTask });
                }
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new ExceptionLocalDB(exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(ex.Message);
            }
        }

        /// <inheritdoc />
        public override async Task<ClassDataCommon> LoadAsync(int id, CancellationToken token = default)
        {
            try
            {
                using (G81Context g81 = new G81Context())
                {
                    await g81.SaveChangesAsync();
                    DbSet<ZadachaEmo> Table = g81.GetTable<ZadachaEmo>(Name);
                    await g81.Entry(Table.OrderByDescending(p => p.IdTaskEx).FirstOrDefault()).ReloadAsync();
                    var lastIdTask = Table.OrderByDescending(p => p.IdTaskEx)
                        .FirstOr(pe => pe.Status == 1, Table.OrderByDescending(p => p.IdTaskEx).FirstOrDefault());
                    return ClassDataCommon.ConvertToListAbstractCommonTable(new List<ZadachaEmo> { lastIdTask });
                }
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new ExceptionLocalDB(exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(ex.Message);
            }
        }
    }
}
