﻿using Microsoft.EntityFrameworkCore;
using PelengatorModelsDbLib;
using PelengatorModelsDbLib.ContextModels;
using System;
using System.Linq;
using System.Net;

namespace KvetkaDbPelengator
{
    using PelengatorModelsDbLib.Context;
    using PelengatorModelsDbLib.ModelsForServer;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;

    public class OperationTableFhssSeans<T> : OperationTable<FhssSeans>
    {
        public OperationTableFhssSeans()
        {
            object[] attrs = typeof(T).GetCustomAttributes(typeof(InfoTableAttribute), false);
            foreach (InfoTableAttribute info in attrs)
            {
                Name = info.Name;
            }
        }

        public OperationTableFhssSeans(
            IPAddress address = default,
            string databaseName = "G81",
            string login = "g81",
            string password = "G81")
        {
            object[] attrs = typeof(T).GetCustomAttributes(typeof(InfoTableAttribute), false);
            foreach (InfoTableAttribute info in attrs)
            {
                Name = info.Name;
            }
        }

        public override ClassDataCommon Load(int id)
        {
            try
            {
                using (G81Context g81 = new G81Context())
                {
                    DbSet<FhssSeans> Table = g81.GetTable<FhssSeans>(Name);
                    var result = Table.Where(t => t.IdTaskEx == id).ToList();
                    if (result.Count != 0)
                    {
                        List<FhssSeansServer> ServerList = new List<FhssSeansServer>();
                        foreach (var server in result)
                        {
                            var temp = new FhssSeansServer();
                            temp.Update(server);
                            ServerList.Add(temp);
                        }
                        return ClassDataCommon.ConvertToListAbstractCommonTable(ServerList);
                    }
                    return ClassDataCommon.ConvertToListAbstractCommonTable(result);
                }
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new ExceptionLocalDB(exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(ex.Message);
            }
        }

        /// <inheritdoc />
        public override async Task<ClassDataCommon> LoadAsync(int id, CancellationToken token = default)
        {
            try
            {
                using (G81Context g81 = new G81Context())
                {
                    DbSet<FhssSeans> Table = g81.GetTable<FhssSeans>(Name);
                    var result = Table.Where(t => t.IdTaskEx == id).ToList();
                    if (result.Count != 0)
                    {
                        List<FhssSeansServer> ServerList = new List<FhssSeansServer>();
                        foreach (var server in result)
                        {
                            var temp = new FhssSeansServer();
                            temp.Update(server);
                            ServerList.Add(temp);
                        }
                        return ClassDataCommon.ConvertToListAbstractCommonTable(ServerList);
                    }
                    return ClassDataCommon.ConvertToListAbstractCommonTable(result);
                }
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new ExceptionLocalDB(exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(ex.Message);
            }
        }
    }
}