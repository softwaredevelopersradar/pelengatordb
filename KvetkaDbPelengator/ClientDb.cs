﻿using PelengatorModelsDbLib;
using System.Collections.Generic;

namespace KvetkaDbPelengator
{
    using System.Net;

    using PelengatorModelsDbLib.ContextModels;

    public class ClientDb
    {
        private readonly IPAddress address;

        private readonly string databaseName;

        private readonly string login;

        private readonly string password;

        public ClientDb(
            IPAddress address = default,
            string databaseName = "G81",
            string login = "g81",
            string password = "G81")
        {
            this.address = address;
            this.databaseName = databaseName;
            this.login = login;
            this.password = password;
            this.Tables = new Dictionary<NameTable, ITableAction>()
                              {
                                  {
                                      NameTable.ZadachaEmo,
                                      new OperationTableZadachaEmo<ZadachaEmo>(
                                          this.address,
                                          this.databaseName,
                                          this.login,
                                          this.password)
                                  },
                                  {
                                      NameTable.Diagnistics,
                                      new OperationTable<Diagnostics>(
                                          this.address,
                                          this.databaseName,
                                          this.login,
                                          this.password)
                                  },
                                  {
                                      NameTable.Servers,
                                      new OperationTable<Servers>(
                                          this.address,
                                          this.databaseName,
                                          this.login,
                                          this.password)
                                  },
                                  {
                                      NameTable.Fhss_Seans,
                                      new OperationTableFhssSeans<FhssSeans>(
                                          this.address,
                                          this.databaseName,
                                          this.login,
                                          this.password)
                                  },
                                  {
                                      NameTable.Fhss_IRI,
                                      new OperationTableFhssIri<FhssIri>(
                                          this.address,
                                          this.databaseName,
                                          this.login,
                                          this.password)
                                  },
                                  {
                                      NameTable.Fhss_IRI_Loacation,
                                      new OperationTableFhssIri<FhssIriLocation>(
                                          this.address,
                                          this.databaseName,
                                          this.login,
                                          this.password)
                                  }

            };

        }

        public Dictionary<NameTable, ITableAction> Tables { get; private set; }
    }
}
