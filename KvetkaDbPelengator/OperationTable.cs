﻿using Microsoft.EntityFrameworkCore;
using PelengatorModelsDbLib;
using System;
using System.Collections.Generic;

namespace KvetkaDbPelengator
{
    using PelengatorModelsDbLib.Context;
    using System.Net;
    using System.Threading;
    using System.Threading.Tasks;

    public class OperationTable<T> : Operation, ITableAction where T : AbstractModel
    {

        public OperationTable()
        {
            object[] attrs = typeof(T).GetCustomAttributes(typeof(InfoTableAttribute), false);
            foreach (InfoTableAttribute info in attrs)
            {
                Name = info.Name;
            }
        }

        public OperationTable(
            IPAddress address = default,
            string databaseName = "G81",
            string login = "g81",
            string password = "G81")
        {
            object[] attrs = typeof(T).GetCustomAttributes(typeof(InfoTableAttribute), false);
            foreach (InfoTableAttribute info in attrs)
            {
                Name = info.Name;
            }
        }

        public virtual bool Add(AbstractModel record)
        {
            try
            {
                using (G81Context g81 = new G81Context())
                {
                    DbSet<T> Table = g81.GetTable<T>(Name);

                    if (!g81.Database.CanConnect())
                    {
                        throw new ExceptionClient(EnumClientError.NoConnection);
                    }

                    if (Table.Find(record.GetKey()) != null)
                    {
                        throw new ExceptionLocalDB(EnumDBError.RecordExist);
                    }

                    T result = record as T;

                    _ = Table.Add(result);
                    _ = g81.SaveChanges();

                }
                return true;
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (ExceptionClient exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new ExceptionLocalDB(exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(ex.Message);
            }
        }

        public virtual async Task<bool> AddAsync(AbstractModel record, CancellationToken token = default)
        {
            try
            {
                using (G81Context g81 = new G81Context())
                {
                    DbSet<T> Table = g81.GetTable<T>(Name);

                    if (!await g81.Database.CanConnectAsync(token).ConfigureAwait(false))
                    {
                        throw new ExceptionClient(EnumClientError.NoConnection);
                    }

                    if (await Table.FindAsync(record.GetKey(), token).ConfigureAwait(false) != null)
                    {
                        throw new ExceptionLocalDB(EnumDBError.RecordExist);
                    }

                    T result = record as T;


                    await Table.AddAsync(result, token).ConfigureAwait(false);
                    await g81.SaveChangesAsync(token).ConfigureAwait(false);
                    return true;
                }
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (ExceptionClient exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new ExceptionLocalDB(exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(ex.Message);
            }
        }

        public virtual bool Change(AbstractModel record)
        {
            try
            {
                using (G81Context g81 = new G81Context())
                {
                    DbSet<T> Table = g81.GetTable<T>(Name);

                    T rec = Table.Find(record.GetKey());
                    if (rec == null)
                        throw new ExceptionLocalDB(EnumDBError.RecordNotFound);

                    (rec as AbstractModel).Update(record);
                    Table.Update(rec);
                    g81.SaveChanges();
                    return true;
                }
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new ExceptionLocalDB(exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(ex.Message);
            }
        }

        public virtual async Task<bool> ChangeAsync(AbstractModel record, CancellationToken token = default)
        {
            try
            {
                using (G81Context g81 = new G81Context())
                {
                    DbSet<T> Table = g81.GetTable<T>(Name);

                    T rec = await Table.FindAsync(record.GetKey()).ConfigureAwait(false);
                    if (rec == null)
                        throw new ExceptionLocalDB(EnumDBError.RecordNotFound);

                    (rec as AbstractModel).Update(record);
                    Table.Update(rec);
                    return Convert.ToBoolean(await g81.SaveChangesAsync());
                }
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new ExceptionLocalDB(exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(ex.Message);
            }
        }

        public virtual ClassDataCommon Load(int id)
        {
            try
            {
                using (G81Context g81 = new G81Context())
                {
                    DbSet<T> Table = g81.GetTable<T>(Name);
                    return ClassDataCommon.ConvertToListAbstractCommonTable(new List<T> { Table.Find(id) });
                }
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new ExceptionLocalDB(exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(ex.Message);
            }
        }


        public virtual async Task<ClassDataCommon> LoadAsync(int id, CancellationToken token = default)
        {
            try
            {
                using (G81Context g81 = new G81Context())
                {
                    DbSet<T> Table = g81.GetTable<T>(Name);

                    return ClassDataCommon.ConvertToListAbstractCommonTable(
                        new List<T> { await Table.FindAsync(id).ConfigureAwait(false) });
                }

            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new ExceptionLocalDB(exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(ex.Message);
            }
        }
    }
}
