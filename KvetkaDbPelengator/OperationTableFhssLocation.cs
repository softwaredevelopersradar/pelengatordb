﻿using Microsoft.EntityFrameworkCore;
using PelengatorModelsDbLib;
using PelengatorModelsDbLib.ContextModels;
using System;
using System.Linq;
using System.Net;

namespace KvetkaDbPelengator
{
    using PelengatorModelsDbLib.Context;
    using System.Threading;
    using System.Threading.Tasks;
    public class OperationTableFhssLocation<T> : OperationTable<FhssIriLocation>
    {
        public OperationTableFhssLocation()
        {
            object[] attrs = typeof(T).GetCustomAttributes(typeof(InfoTableAttribute), false);
            foreach (InfoTableAttribute info in attrs)
            {
                Name = info.Name;
            }
        }

        public OperationTableFhssLocation(
            IPAddress address = default,
            string databaseName = "G81",
            string login = "g81",
            string password = "G81")
        {
            object[] attrs = typeof(T).GetCustomAttributes(typeof(InfoTableAttribute), false);
            foreach (InfoTableAttribute info in attrs)
            {
                Name = info.Name;
            }
        }

        public override ClassDataCommon Load(int id)
        {
            try
            {
                using (G81Context g81 = new G81Context())
                {
                    DbSet<FhssIriLocation> Table = g81.GetTable<FhssIriLocation>(Name);

                    return ClassDataCommon.ConvertToListAbstractCommonTable(Table.Where(t => t.IdTaskEx == id).ToList());
                }

            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new ExceptionLocalDB(exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(ex.Message);
            }
        }

        /// <inheritdoc />
        public override async Task<ClassDataCommon> LoadAsync(int id, CancellationToken token = default)
        {
            try
            {
                using (G81Context g81 = new G81Context())
                {
                    DbSet<FhssIriLocation> Table = g81.GetTable<FhssIriLocation>(Name);
                    return ClassDataCommon.ConvertToListAbstractCommonTable(Table.Where(t => t.IdTaskEx == id).ToList());
                }

            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new ExceptionLocalDB(exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(ex.Message);
            }
        }
    }
}